// = ../../node_modules/jquery/dist/jquery.js

// Animate miami section
function animateMiami() {
    var el = document.getElementById('miami-main');
    if (typeof(el) != 'undefined' && el != null) {
        var list = document.querySelectorAll(".advantages-miami__item");
        var topPos = el.offsetTop;

        window.onscroll = function () {
            var scrolled = window.pageYOffset;
            if (scrolled > (topPos - 500)) {
                var index = 0;
                var interval = setInterval(function () {
                    list[index++].classList.add("show");
                    if (index === list.length) {
                        clearInterval(interval);
                    }
                }, 300);
            }

            if (window.width < 767) {
                if (scrolled > (topPos - 900)) {
                    var index = 0;
                    var interval = setInterval(function () {
                        list[index++].classList.add("show");
                        if (index === list.length) {
                            clearInterval(interval);
                        }
                    }, 300);
                }
            }
        }
    }
}

animateMiami();
// Animate miami section

var companyTrustList = new Swiper('.company-trust__list', {
    slidesPerView: 'auto',
    loopedSlides: true,
    spaceBetween: 37,
    breakpoints: {
        560: {
            slidesPerView: 'auto',
            spaceBetween: 37,
            loopedSlides: true,
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
                clickable: true
            }
        }
    }
});

var advantagesUsa = new Swiper('#advantages-usa', {
    slidesPerView: 3,
    breakpoints: {
        992: {
            slidesPerView: 'auto',
            spaceBetween: 15
        },
        560: {
            slidesPerView: 'auto',
            centeredSlides: true,
            slidesPerGroup: 1,
            loop: true,
            spaceBetween: 15
        }
    }
});





function initgallery() {
    var width = window.innerWidth;

    if (width > 992) {
        var galleryThumbs = new Swiper('.hospital-gallery_thumbs', {
            spaceBetween: 15,
            slidesPerView: 5,
            loop: true,
            freeMode: true,
            loopedSlides: 10,
            watchSlidesVisibility: true,
            watchSlidesProgress: true,
        });

        var galleryTop = new Swiper('.hospital-gallery_full', {
            spaceBetween: 10,
            loop:true,
            loopedSlides: 10,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            thumbs: {
                swiper: galleryThumbs,
            },
        });
    }
}

initgallery();




var currentDataIndex;

var parentExperience = new Swiper('#parent-experience', {
    slidesPerView: 1,
    loop: true,
    effect: 'fade',
    delay: 3000,
    breakpoints: {
        1279: {
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
                clickable: true
            }
        }
    },
    on: {
        slideChangeTransitionEnd: function () {
            currentDataIndex = document.querySelector('.parent-experience .swiper-slide-active').getAttribute('data-slide');
        }
    }
});

function getWindowWidth() {
    var width = window.innerWidth;
    if (width < 1279 && document.getElementById('parent-experience') != null) {
        document.getElementById('parent-experience').classList.remove('swiper-no-swiping');
    }
}

getWindowWidth();

if (document.getElementById('parent-experience')!=null) {
    var countSlides = parentExperience.slides.length - 2
}


function nextSlide() {
    parentExperience.slideNext();
    nextDataIndex = parseInt(currentDataIndex) + 2;
    if (nextDataIndex == countSlides + 1) {
        nextDataIndex = 1
    }
    if (nextDataIndex == countSlides + 2) {
        nextDataIndex = 2
    }

    var image = document.querySelector("[data-image='" + nextDataIndex + "']");
    var cloneImage = image.cloneNode(true);

    var nextImageContainer = document.querySelector(".parent-experience__next-slide");
    var nextImage = document.querySelector(".parent-experience__next-slide img");

    nextImage.classList.add('hide');

    if ( document.querySelectorAll('.parent-experience__next-slide .show').length == 1 ) {
        nextImageContainer.append(cloneImage);
    }

    setTimeout(function () {
        cloneImage.classList.add('show');
    }, 50);

    setTimeout(function () {
        nextImage.remove();
    }, 300);
}

var thumbnail = document.getElementsByClassName('parent-experience__thumbnail-image');
for (i = 0; i < thumbnail.length; i++) {
    thumbnail[i].addEventListener('click', function () {
        var elems = document.querySelectorAll(".parent-experience__thumbnail-image.active");
        [].forEach.call(elems, function (el) {
            el.classList.remove("active");
        });
        this.classList.add('active');
        var currentThumbnail = this.getAttribute('data-slide');
        parentExperience.slideTo(currentThumbnail);
        var nextThumbnail = parseInt(currentThumbnail) + 1;
        if (nextThumbnail == 6) {
            nextThumbnail = 1
        }

        var image = document.querySelector("[data-image='" + nextThumbnail + "']");
        var cloneImage = image.cloneNode(true);
        var nextImageContainer = document.querySelector(".parent-experience__next-slide");
        var nextImage = document.querySelector(".parent-experience__next-slide img");

        nextImage.classList.add('hide');
        setTimeout(function () {
            nextImage.remove();
        }, 300);

        nextImageContainer.append(cloneImage);
        setTimeout(function () {
            cloneImage.classList.add('show');
        }, 50)
    });
}

// lazyload youtube video

(function () {

    var youtube = document.querySelectorAll(".youtube-video__container");

    for (var i = 0; i < youtube.length; i++) {

        var source = "https://img.youtube.com/vi/" + youtube[i].dataset.embed + "/maxresdefault.jpg";

        var image = new Image();
        image.src = source;
        image.addEventListener("load", function () {
            youtube[i].appendChild(image);
        }(i));

        youtube[i].addEventListener("click", function () {

            var iframe = document.createElement("iframe");

            iframe.setAttribute("frameborder", "0");
            iframe.setAttribute("allowfullscreen", "");
            iframe.setAttribute("src", "https://www.youtube.com/embed/" + this.dataset.embed + "?rel=0&showinfo=0&autoplay=1");

            this.innerHTML = "";
            this.appendChild(iframe);
        });
    }

})();
// lazyload youtube video

// read more btn
document.getElementsByClassName('read-more')
// read more btn


new Vue({
    el: '#offices-card-wrap',
    data: {
        activeCard: 1,
        cards: [
            {
                class: 'usa',
                id: 1,
                btnName: 'usa',
                title: 'Центральный офис в USA:',
                address: '2999 NE 191 ST, Suite 408 Aventura, FL 33180',
                mail: 'ukraine@pilot-med.com',
                phone: '+380(44)377-57-57',

                link: 'https://www.google.com.ua/maps/place/2999+NE+191st+St+%23408,+Miami,+FL+33180,+%D0%A1%D0%A8%D0%90/@25.9526878,-80.1439131,17z/data=!3m1!4b1!4m5!3m4!1s0x88d9acf7f5f95ad7:0xbcd81aaaec135309!8m2!3d25.9526878!4d-80.1417244?hl=ru'

            },
            {
                class: 'ua',
                id: 2,
                btnName: 'ua',
                title: 'Центральный офис в Украине:',
                address: 'Киев, ул. Предславинская, 49',
                mail: 'ukraine@pilot-med.com',
                phone: '+380(44)377-57-57',

                link: 'https://www.google.com.ua/maps/place/%D1%83%D0%BB.+%D0%9F%D1%80%D0%B5%D0%B4%D1%81%D0%BB%D0%B0%D0%B2%D0%B8%D0%BD%D1%81%D0%BA%D0%B0%D1%8F,+49,+%D0%9A%D0%B8%D0%B5%D0%B2,+02000/@50.4180815,30.5236049,17z/data=!3m1!4b1!4m5!3m4!1s0x40d4cf3d86777b21:0x1af9bbbde91983e2!8m2!3d50.4180815!4d30.5257936?hl=ru'

            }
        ]
    },
    methods: {
        selectCard: function (id) {
            this.activeCard = id;
        }
    }

});

// google maps
var map;
var marker;

var siteUrl = document.location.origin;

var iconBase = siteUrl + '/childbirdth/img/map-marker.svg';

var styleMap = [
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#e9e9e9"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f5f5f5"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 29
            },
            {
                "weight": 0.2
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 18
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f5f5f5"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#dedede"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#ffffff"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "saturation": 36
            },
            {
                "color": "#333333"
            },
            {
                "lightness": 40
            }
        ]
    },
    {
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f2f2f2"
            },
            {
                "lightness": 19
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#fefefe"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#fefefe"
            },
            {
                "lightness": 17
            },
            {
                "weight": 1.2
            }
        ]
    }
];

var mapCenter = {lat: 50.387250, lng: 30.611240};

function toggleMap(el) {
    var data = el.getAttribute('data-id');
    if (data == 1) {
        mapCenter = {lat: 25.952650, lng: -80.141360};
    } else {
        mapCenter = {lat: 50.387250, lng: 30.611240};
    }
    initMap();
}

var cards = document.querySelectorAll(".offices-card");

for (var i = 0; i < cards.length; i++) {
    cards[i].addEventListener('click', function () {
        toggleMap(this);
    });
}

var cardsMobile = document.querySelectorAll(".card-toggle__item");

for (var j = 0; j < cardsMobile.length; j++) {
    cardsMobile[j].addEventListener('click', function () {
        toggleMap(this);
    });
}

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: mapCenter,
        zoom: 17,
        mapTypeId: 'styleMap'
    });

    map.mapTypes.set('styleMap', new google.maps.StyledMapType(styleMap, {name: 'My Style'}))

    marker = new google.maps.Marker({
        position: map.getCenter(),
        icon: iconBase,
        map: map
    });

    map2 = new google.maps.Map(document.getElementById('map_mobile'), {
        center: mapCenter,
        zoom: 17,
        mapTypeId: 'styleMap'
    });

    map2.mapTypes.set('styleMap', new google.maps.StyledMapType(styleMap, {name: 'My Style'}))

    marker = new google.maps.Marker({
        position: map.getCenter(),
        icon: iconBase,
        map: map2
    });
}

// google maps



new Vue({
    el: '.header-wrap',
    data: {
        sticky: window.scrollY,
        headerScrolltop: 0,
        open: 0,

        showSubMenu: '',
        mobileHeaderItems: [
            {
                id: 1,
                title: 'РОДЫ В США',
                sumbenu: [
                    {submenuItem: 'Общая медицина'},
                    {submenuItem: 'Репродуктивная медицина'},
                    {submenuItem: 'Эстетическая медицина'},
                    {submenuItem: 'Госпитали'},
                    {submenuItem: 'Врачи'},
                    {submenuItem: 'Дополнительные услуги'}
                ]
            },
            {
                id:2,
                title: 'МЕДИЦИНА',
                submenu: [
                    {submenuItem: 'Общая медицина'},
                    {submenuItem: 'Репродуктивная медицина'},
                    {submenuItem: 'Эстетическая медицина'},
                    {submenuItem: 'Госпитали'},
                    {submenuItem: 'Врачи'},
                    {submenuItem: 'Дополнительные услуги'}
                ]
            },
            {
                id:3,
                title: 'ЦЕНЫ',
                submenu: [
                    {submenuItem: 'Общая медицина'},
                    {submenuItem: 'Репродуктивная медицина'},
                    {submenuItem: 'Эстетическая медицина'},
                    {submenuItem: 'Госпитали'},
                    {submenuItem: 'Врачи'},
                    {submenuItem: 'Дополнительные услуги'}
                ]
            },
            {
                id:4,
                title: 'ВАЖНО ЗНАТЬ',
                submenu: [
                    {submenuItem: 'Общая медицина'},
                    {submenuItem: 'Репродуктивная медицина'},
                    {submenuItem: 'Эстетическая медицина'},
                    {submenuItem: 'Госпитали'},
                    {submenuItem: 'Врачи'},
                    {submenuItem: 'Дополнительные услуги'}
                ]
            },
            {
                id:5,
                title: 'O PILOT-MED',
                submenu: [
                    {submenuItem: 'Общая медицина'},
                    {submenuItem: 'Репродуктивная медицина'},
                    {submenuItem: 'Эстетическая медицина'},
                    {submenuItem: 'Госпитали'},
                    {submenuItem: 'Врачи'},
                    {submenuItem: 'Дополнительные услуги'}
                ]
            }
        ]
    },

    methods: {
        handleScroll: function () {
            this.headerScrolltop = window.scrollY;
            if (this.headerScrolltop > 1 && this.open == false) {
                this.sticky = true
            } else {
                this.sticky = false
            }
        },
        toggleMenu: function () {
            this.open = !this.open;
        },
        toggleSubMenu: function (id) {
            this.showSubMenu = id;
            // if (this.showSubMenu == id) {
            //     console.log(11)
            // }
        }
    },
    created: function () {
        window.addEventListener('scroll', this.handleScroll);
    },
    destroyed: function () {
        window.removeEventListener('scroll', this.handleScroll);
    }
});


new Vue({
    el: '.childbirth-accordion',
    data: {
        // activeItem: null,
        items: [
            {
                show: true,

                id: 1,
                iconSrc: 'img/1_objects_3.svg',
                title: 'Встреча с Pilot-Med и подготовка к поездке',
                itemList: [
                    {itemName: 'Консультация и подбор подходящего именно вам пакета услуг'},
                    {itemName: 'Выбор доктора, который будет сопровождать беременность и принимать роды'},
                    {itemName: 'Подбор оптимальной клиники для родов'},
                    {itemName: 'Выбор района проживания и подбор апартаментов'},
                    {itemName: 'Подписание договора аренды и всех необходимых документов на апартаменты, оплата депозита'},
                    {itemName: 'Подбор автомобиля, оформление договора аренды, оплата депозита'},
                    {itemName: 'Определение даты вылета, подбор оптимальных авиаперелетов и бронирование билетов'},
                    {itemName: 'Составление оптимального бюджета'},
                    {itemName: 'Перевод на английский язык пакета медицинских документов и анализов'},
                    {itemName: 'Оформление медицинской страховки'},
                    {itemName: 'Подготовка справки для авиакомпании о сроке беременности и отсутствии противопоказаний для перелета'},
                ]
            },
            {
                show:false,

                id: 2,
                iconSrc: 'img/1_objects_4.svg',
                title: 'Получение визы ',
                itemList: [
                    {itemName: 'Консультация и подбор подходящего именно вам пакета услуг'},
                    {itemName: 'Выбор доктора, который будет сопровождать беременность и принимать роды'},
                    {itemName: 'Подбор оптимальной клиники для родов'},
                    {itemName: 'Выбор района проживания и подбор апартаментов'},
                    {itemName: 'Подписание договора аренды и всех необходимых документов на апартаменты, оплата депозита'},
                    {itemName: 'Подбор автомобиля, оформление договора аренды, оплата депозита'},
                    {itemName: 'Определение даты вылета, подбор оптимальных авиаперелетов и бронирование билетов'},
                    {itemName: 'Составление оптимального бюджета'},
                    {itemName: 'Перевод на английский язык пакета медицинских документов и анализов'},
                    {itemName: 'Оформление медицинской страховки'},
                    {itemName: 'Подготовка справки для авиакомпании о сроке беременности и отсутствии противопоказаний для перелета'},
                ]
            },
            {
                show:false,

                id: 3,
                iconSrc: 'img/1_objects_5.svg',
                title: 'Перелет',
                itemList: [
                    {itemName: 'Консультация и подбор подходящего именно вам пакета услуг'},
                    {itemName: 'Выбор доктора, который будет сопровождать беременность и принимать роды'},
                    {itemName: 'Подбор оптимальной клиники для родов'},
                    {itemName: 'Выбор района проживания и подбор апартаментов'},
                    {itemName: 'Подписание договора аренды и всех необходимых документов на апартаменты, оплата депозита'},
                    {itemName: 'Подбор автомобиля, оформление договора аренды, оплата депозита'},
                    {itemName: 'Определение даты вылета, подбор оптимальных авиаперелетов и бронирование билетов'},
                    {itemName: 'Составление оптимального бюджета'},
                    {itemName: 'Перевод на английский язык пакета медицинских документов и анализов'},
                    {itemName: 'Оформление медицинской страховки'},
                    {itemName: 'Подготовка справки для авиакомпании о сроке беременности и отсутствии противопоказаний для перелета'},
                ]
            },
            {
                show:false,

                id: 4,
                iconSrc: 'img/1_objects_6.svg',
                title: 'До родов',
                itemList: [
                    {itemName: 'Консультация и подбор подходящего именно вам пакета услуг'},
                    {itemName: 'Выбор доктора, который будет сопровождать беременность и принимать роды'},
                    {itemName: 'Подбор оптимальной клиники для родов'},
                    {itemName: 'Выбор района проживания и подбор апартаментов'},
                    {itemName: 'Подписание договора аренды и всех необходимых документов на апартаменты, оплата депозита'},
                    {itemName: 'Подбор автомобиля, оформление договора аренды, оплата депозита'},
                    {itemName: 'Определение даты вылета, подбор оптимальных авиаперелетов и бронирование билетов'},
                    {itemName: 'Составление оптимального бюджета'},
                    {itemName: 'Перевод на английский язык пакета медицинских документов и анализов'},
                    {itemName: 'Оформление медицинской страховки'},
                    {itemName: 'Подготовка справки для авиакомпании о сроке беременности и отсутствии противопоказаний для перелета'},
                ]
            },
            {
                show:false,

                id: 5,
                iconSrc: 'img/1_objects_7.svg',
                title: 'Роды',
                itemList: [
                    {itemName: 'Консультация и подбор подходящего именно вам пакета услуг'},
                    {itemName: 'Выбор доктора, который будет сопровождать беременность и принимать роды'},
                    {itemName: 'Подбор оптимальной клиники для родов'},
                    {itemName: 'Выбор района проживания и подбор апартаментов'},
                    {itemName: 'Подписание договора аренды и всех необходимых документов на апартаменты, оплата депозита'},
                    {itemName: 'Подбор автомобиля, оформление договора аренды, оплата депозита'},
                    {itemName: 'Определение даты вылета, подбор оптимальных авиаперелетов и бронирование билетов'},
                    {itemName: 'Составление оптимального бюджета'},
                    {itemName: 'Перевод на английский язык пакета медицинских документов и анализов'},
                    {itemName: 'Оформление медицинской страховки'},
                    {itemName: 'Подготовка справки для авиакомпании о сроке беременности и отсутствии противопоказаний для перелета'},
                ]
            },
            {
                show:false,

                id: 6,
                iconSrc: 'img/1_objects_8.svg',
                title: 'После родов ',
                itemList: [
                    {itemName: 'Консультация и подбор подходящего именно вам пакета услуг'},
                    {itemName: 'Выбор доктора, который будет сопровождать беременность и принимать роды'},
                    {itemName: 'Подбор оптимальной клиники для родов'},
                    {itemName: 'Выбор района проживания и подбор апартаментов'},
                    {itemName: 'Подписание договора аренды и всех необходимых документов на апартаменты, оплата депозита'},
                    {itemName: 'Подбор автомобиля, оформление договора аренды, оплата депозита'},
                    {itemName: 'Определение даты вылета, подбор оптимальных авиаперелетов и бронирование билетов'},
                    {itemName: 'Составление оптимального бюджета'},
                    {itemName: 'Перевод на английский язык пакета медицинских документов и анализов'},
                    {itemName: 'Оформление медицинской страховки'},
                    {itemName: 'Подготовка справки для авиакомпании о сроке беременности и отсутствии противопоказаний для перелета'},
                ]
            }

        ],
    },
    methods: {
        selectItem: function () {
            // this.activeItem = id;
            // if (this.activeItem == id) {
            //     if (this.classList.contains('active')) {
            //         console.log(1)
            //         this.activeItem = null
            //     }
            // }
            // this.item.activeItem = !this.item.activeItem

            // console.log(this.activeItem)

        }
    }

});

new Vue({
    el: '.medecine-main',
    data: {
        activeDesktop: 0,
        activeMobile: 0,
        items: [
            {
                open: true,

                id: 1,
                imgSrc: '<?xml version="1.0" encoding="UTF-8"?>\n' +
                    '<svg width="61px" height="63px" viewBox="0 0 61 63" version="1.1" xmlns="http://www.w3.org/2000/svg"\n' +
                    '     xmlns:xlink="http://www.w3.org/1999/xlink">\n' +
                    '    <title>Group 3</title>\n' +
                    '    <desc>Created with Sketch.</desc>\n' +
                    '    <defs>\n' +
                    '        <polygon id="path-1" points="0 0.0002 60.873 0.0002 60.873 62.2 0 62.2"></polygon>\n' +
                    '\n' +
                    '    </defs>\n' +
                    '    <g id="main-landing" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\n' +
                    '        <g id="Main_Med" transform="translate(-1030.000000, -638.000000)">\n' +
                    '            <g id="Medecines" transform="translate(75.000000, 585.000000)">\n' +
                    '                <g id="Group-3" transform="translate(955.000000, 53.000000)">\n' +
                    '                    <mask id="mask-2" fill="white">\n' +
                    '                        <use xlink:href="#path-1"></use>\n' +
                    '                    </mask>\n' +
                    '                    <g id="Clip-2"></g>\n' +
                    '                    <path d="M51.2622,3.439 C51.8752,5.169 52.6302,6.49 53.5062,7.367 C54.3832,8.243 55.7032,8.999 57.4342,9.612 C58.5362,7.865 59.0232,5.771 58.8042,3.705 C58.7132,2.847 58.0262,2.159 57.1682,2.069 C56.8382,2.034 56.5092,2.017 56.1822,2.017 C54.4342,2.017 52.7352,2.508 51.2622,3.439 M8.7932,52.822 C9.1462,52.822 9.4782,53.011 9.6582,53.314 C9.7962,53.544 9.8362,53.815 9.7712,54.076 C9.7052,54.338 9.5412,54.558 9.3112,54.696 C7.2842,55.907 5.1222,56.938 2.8832,57.759 L2.1392,60.059 C7.2942,58.288 12.0502,55.319 15.9032,51.467 L24.2942,43.075 C24.5202,42.849 24.7162,42.594 24.8782,42.318 L25.6732,40.957 C25.9202,40.533 26.2232,40.141 26.5732,39.791 L27.5622,38.802 C26.9052,38.979 26.3072,39.324 25.8192,39.812 L16.6752,48.956 C15.4982,50.133 14.2312,51.243 12.9072,52.257 C12.7302,52.393 12.5192,52.464 12.2952,52.464 C11.9782,52.464 11.6862,52.32 11.4942,52.068 C11.3302,51.855 11.2592,51.59 11.2952,51.323 C11.3302,51.057 11.4682,50.819 11.6822,50.655 C12.9362,49.695 14.1362,48.644 15.2502,47.53 L18.3212,44.459 C18.2572,44.409 18.1962,44.356 18.1382,44.297 L16.4162,42.575 C15.9902,42.149 15.7542,41.581 15.7542,40.977 C15.7542,40.373 15.9902,39.805 16.4162,39.379 L20.9772,34.818 C21.6562,34.139 22.0622,33.219 22.1132,32.265 L9.4102,44.967 C7.2852,47.092 5.6502,49.722 4.6832,52.571 L3.7772,55.242 C5.3302,54.587 6.8432,53.822 8.2762,52.965 C8.4332,52.872 8.6122,52.822 8.7932,52.822 M1.8032,62.2 C1.3222,62.2 0.8672,62.01 0.5252,61.665 C0.0392,61.176 -0.1248,60.466 0.0972,59.813 L2.7742,51.923 C3.8382,48.785 5.6402,45.886 7.9852,43.542 L42.6012,8.926 C42.7912,8.735 43.0442,8.63 43.3142,8.63 C43.5832,8.63 43.8362,8.735 44.0262,8.926 C44.2162,9.116 44.3222,9.369 44.3222,9.639 C44.3222,9.908 44.2162,10.161 44.0262,10.352 L41.4072,12.971 L42.6552,14.22 C43.0482,14.613 43.0482,15.253 42.6552,15.646 C42.4642,15.836 42.2102,15.941 41.9422,15.941 C41.6722,15.941 41.4192,15.836 41.2292,15.646 L39.9812,14.397 L38.1592,16.218 L39.4082,17.466 C39.5992,17.657 39.7032,17.91 39.7032,18.18 C39.7032,18.449 39.5992,18.702 39.4082,18.893 C39.2182,19.083 38.9652,19.188 38.6962,19.188 C38.4272,19.188 38.1732,19.083 37.9832,18.893 L36.7342,17.644 L34.9132,19.464 L36.1612,20.713 C36.5552,21.106 36.5552,21.746 36.1612,22.139 C35.9712,22.329 35.7172,22.434 35.4492,22.434 C35.1792,22.434 34.9262,22.329 34.7362,22.139 L33.4882,20.89 L24.9432,29.434 C24.3642,30.014 24.0632,30.815 24.1192,31.633 C24.2402,33.358 23.6132,35.032 22.4022,36.243 L17.8412,40.804 C17.7462,40.899 17.7462,41.054 17.8412,41.148 L19.5632,42.871 C19.6102,42.917 19.6712,42.942 19.7362,42.942 C19.8012,42.942 19.8632,42.917 19.9082,42.871 L24.3942,38.386 C25.5112,37.268 26.9942,36.652 28.5712,36.652 C28.7792,36.652 28.9882,36.663 29.1982,36.685 C29.2972,36.695 29.3962,36.701 29.4942,36.701 C29.6442,36.701 29.7922,36.689 29.9392,36.666 C30.5672,36.555 31.0852,36.283 31.4982,35.871 L39.9832,27.386 L38.7352,26.137 C38.3412,25.744 38.3412,25.105 38.7352,24.711 C38.9242,24.521 39.1782,24.416 39.4472,24.416 C39.7162,24.416 39.9702,24.521 40.1592,24.711 L41.4082,25.96 L43.2292,24.14 L41.9812,22.891 C41.7902,22.7 41.6852,22.447 41.6852,22.178 C41.6852,21.908 41.7902,21.655 41.9812,21.465 C42.1702,21.274 42.4242,21.17 42.6942,21.17 C42.9622,21.17 43.2162,21.274 43.4072,21.465 L44.6562,22.713 L46.4762,20.893 L45.2272,19.645 C44.8332,19.252 44.8332,18.612 45.2272,18.219 C45.4172,18.028 45.6702,17.923 45.9402,17.923 C46.2092,17.923 46.4622,18.028 46.6532,18.219 L47.9022,19.467 L56.1032,11.266 C54.4012,10.592 53.0482,9.76 52.0812,8.793 C51.1142,7.826 50.2822,6.473 49.6082,4.772 L47.0252,7.352 C46.8352,7.543 46.5812,7.647 46.3132,7.647 C46.0432,7.647 45.7892,7.543 45.6002,7.352 C45.2062,6.959 45.2062,6.319 45.6002,5.926 L48.2242,3.302 C50.3542,1.173 53.1792,0 56.1782,0 C56.5772,0 56.9792,0.021 57.3812,0.064 C59.1772,0.254 60.6192,1.696 60.8102,3.492 C61.1692,6.894 59.9892,10.231 57.5712,12.65 L32.9232,37.297 C32.2822,37.939 31.4802,38.386 30.6032,38.59 L27.9982,41.217 C27.7732,41.443 27.5752,41.698 27.4132,41.974 L26.6182,43.335 C26.3702,43.76 26.0682,44.152 25.7192,44.502 L17.3272,52.893 C14.8082,55.413 11.9382,57.567 8.7922,59.302 L32.8642,59.302 C33.4192,59.302 33.8712,59.754 33.8712,60.311 C33.8712,60.867 33.4192,61.319 32.8642,61.319 L4.5152,61.319 C3.8252,61.603 3.0922,61.871 2.3732,62.107 C2.1882,62.169 1.9952,62.2 1.8032,62.2"\n' +
                    '                          id="Fill-1" fill="rgb(130, 210, 227)" mask="url(#mask-2)"></path>\n' +
                    '                </g>\n' +
                    '            </g>\n' +
                    '        </g>\n' +
                    '    </g>\n' +
                    '</svg>',
                title: 'Пластическая хирургия',

                tabText: 'США является лидером по количеству пластических операций, а специалисты сферы разрабатывают новые технологии, которые позволяют сделать операции менее инвазивными и более эффективными.\n' +
                    '\n' +
                    'Майами - один из центров пластической хирургии США. Здесь всегда летняя погода и изобилие пляжей. Люди проводят максимум времени с минимумом одежды, а значит вопросы красоты и эстетики становятся более актуальными.',
                tabSubTitle: 'Пластическая медицина в Майами это:',

                tabList: [
                    {tabListItem: 'Тщательное обследование перед операцией, применение современных технологий и качественное лечение минимизирует риски для пациентов;'},
                    {tabListItem: 'Защита интересов каждого пациента законами о контроле качества страны и штата Флорида;'}
                ],

            },
            {
                open: false,

                id: 2,
                imgSrc: '<?xml version="1.0" encoding="UTF-8"?>\n' +
                    '<svg width="50px" height="61px" viewBox="0 0 50 61" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">\n' +
                    '    <!-- Generator: Sketch 52.2 (67145) - http://www.bohemiancoding.com/sketch -->\n' +
                    '    <title>Group 3</title>\n' +
                    '    <desc>Created with Sketch.</desc>\n' +
                    '    <defs>\n' +
                    '        <polygon id="path-1" points="0 -0.000257746479 49.7603448 -0.000257746479 49.7603448 60.9738817 0 60.9738817"></polygon>\n' +
                    '        <linearGradient x1="63.9594438%" y1="100%" x2="63.9594438%" y2="37.7374922%" id="linearGradient-3">\n' +
                    '            <stop stop-color="#108BBF" offset="0%"></stop>\n' +
                    '            <stop stop-color="#82D2E3" offset="100%"></stop>\n' +
                    '        </linearGradient>\n' +
                    '    </defs>\n' +
                    '    <g id="main-landing" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\n' +
                    '        <g id="Main_Med" transform="translate(-354.000000, -640.000000)">\n' +
                    '            <g id="Medecines" transform="translate(75.000000, 585.000000)">\n' +
                    '                <g id="Group-3" transform="translate(279.000000, 55.000000)">\n' +
                    '                    <mask id="mask-2" fill="white">\n' +
                    '                        <use xlink:href="#path-1"></use>\n' +
                    '                    </mask>\n' +
                    '                    <g id="Clip-2"></g>\n' +
                    '                    <path d="M15.5205172,29.3303465 C15.0041379,29.3303465 14.5843103,28.911938 14.5843103,28.3973042 C14.5843103,27.8826704 15.0041379,27.464262 15.5205172,27.464262 C16.0368966,27.464262 16.4567241,27.8826704 16.4567241,28.3973042 C16.4567241,28.911938 16.0368966,29.3303465 15.5205172,29.3303465 Z M11.2403448,34.0677268 C10.7248276,34.0677268 10.3041379,33.6484592 10.3041379,33.1346845 C10.3041379,32.6200507 10.7248276,32.2016423 11.2403448,32.2016423 C11.7567241,32.2016423 12.1765517,32.6200507 12.1765517,33.1346845 C12.1765517,33.6484592 11.7567241,34.0677268 11.2403448,34.0677268 Z M19.4075862,37.1056986 C18.8912069,37.1056986 18.4713793,36.6872901 18.4713793,36.1726563 C18.4713793,35.6580225 18.8912069,35.2396141 19.4075862,35.2396141 C19.9239655,35.2396141 20.3446552,35.6580225 20.3446552,36.1726563 C20.3446552,36.6872901 19.9239655,37.1056986 19.4075862,37.1056986 Z M12.1196552,40.9117549 C11.6032759,40.9117549 11.1834483,40.4933465 11.1834483,39.9787127 C11.1834483,39.4640789 11.6032759,39.0456704 12.1196552,39.0456704 C12.6351724,39.0456704 13.0558621,39.4640789 13.0558621,39.9787127 C13.0558621,40.4933465 12.6351724,40.9117549 12.1196552,40.9117549 Z M22.7412069,6.70707887 C22.3403448,6.70707887 21.9653448,6.86258592 21.6834483,7.14352958 L20.3308621,8.49068451 C20.0481034,8.77162817 19.892069,9.14707887 19.892069,9.54658592 C19.892069,9.9452338 20.0481034,10.3198254 20.3308621,10.5999099 L39.1317241,29.338938 C39.4144828,29.6198817 39.7894828,29.7753887 40.1903448,29.7753887 C40.5903448,29.7753887 40.9662069,29.6198817 41.2489655,29.338938 L42.6006897,27.9909239 C43.1834483,27.4092761 43.1834483,26.4624873 42.6006897,25.8808394 L23.8006897,7.14352958 C23.517069,6.86258592 23.142069,6.70707887 22.7412069,6.70707887 Z M27.2213793,32.4078394 L33.3308621,26.3190085 L23.3610345,16.3828817 L17.2506897,22.4717127 L27.2213793,32.4078394 Z M8.29206897,31.3991915 C6.43258621,33.2523887 5.76448276,35.9604451 6.54810345,38.4666 C6.7412069,39.0834732 6.67137931,39.7424451 6.35068966,40.3215155 C5.88517241,41.161769 6.03689655,42.2245437 6.7187931,42.9049944 C7.12396552,43.307938 7.68603448,43.5381915 8.26448276,43.5381915 C8.63086207,43.5381915 8.99293103,43.446262 9.31103448,43.2709944 C9.67051724,43.0725296 10.0722414,42.9677127 10.4722414,42.9677127 C10.7084483,42.9677127 10.9437931,43.0037972 11.1713793,43.0742479 C11.8584483,43.2881775 12.5687931,43.396431 13.2834483,43.396431 C15.1610345,43.396431 16.93,42.6652901 18.2636207,41.3370366 L25.8351724,33.7885014 L15.8644828,23.8523746 L8.29206897,31.3991915 Z M8.26706897,45.4893324 C7.17137931,45.4893324 6.10155172,45.0503042 5.33344828,44.2847972 C4.03689655,42.9934873 3.75068966,40.9761915 4.63517241,39.3781634 C4.69465517,39.2690507 4.71017241,39.1530648 4.67741379,39.0482479 C3.67568966,35.8470366 4.53,32.3872197 6.90758621,30.0185296 L21.9748276,15.0005014 L18.9463793,11.981431 C18.292931,11.3310507 17.9334483,10.4658817 17.9334483,9.54658592 C17.9334483,8.62557183 18.292931,7.76126197 18.9463793,7.11088169 L20.2981034,5.76286761 C20.9498276,5.11334648 21.817931,4.75507887 22.742069,4.75507887 C23.6653448,4.75507887 24.5334483,5.11334648 25.1851724,5.76286761 L26.5825862,7.15469859 L30.2351724,3.51531831 C30.4196552,3.3306 30.6653448,3.22921972 30.9274138,3.22921972 C31.1894828,3.22921972 31.4351724,3.3306 31.6205172,3.51445915 C32.0024138,3.89592394 32.0024138,4.51537465 31.6205172,4.89683944 L27.9687931,8.53621972 L41.2032759,21.7259662 L45.0662069,17.8778113 C48.7144828,14.2410085 48.7144828,8.32400845 45.0662069,4.68634648 C43.705,3.33145915 41.9912069,2.43793803 40.1075862,2.10286761 C39.5558621,2.00320563 38.9946552,1.9542338 38.4386207,1.9542338 C37.1386207,1.9542338 35.8739655,2.21885352 34.68,2.74207887 C34.555,2.79620563 34.4222414,2.82369859 34.2868966,2.82369859 C33.8972414,2.82369859 33.5446552,2.59516338 33.3894828,2.24033239 C33.2843103,2.00062817 33.2782759,1.73600845 33.3731034,1.49286761 C33.467931,1.24972676 33.6524138,1.05813521 33.892931,0.95331831 C35.3360345,0.320980282 36.8653448,-0.000343661972 38.4368966,-0.000343661972 C39.1084483,-0.000343661972 39.7868966,0.060656338 40.4524138,0.179219718 C42.7325862,0.586459155 44.8075862,1.66813521 46.4515517,3.30654366 C50.8636207,7.70369859 50.8636207,14.8604592 46.4515517,19.2576141 L42.5894828,23.1074873 L43.9868966,24.5001775 C45.3334483,25.8430366 45.3334483,28.0278676 43.9868966,29.3715859 L42.6334483,30.7187408 C41.9808621,31.3699803 41.1144828,31.7273887 40.1903448,31.7273887 C39.267069,31.7273887 38.3989655,31.3699803 37.7463793,30.7187408 L34.717069,27.7005296 L19.6481034,42.7168394 C17.9455172,44.4153887 15.6851724,45.3501493 13.2851724,45.3501493 C12.3731034,45.3501493 11.4653448,45.2118254 10.5877586,44.9394732 C10.5515517,44.9274451 10.5136207,44.921431 10.4756897,44.921431 C10.4032759,44.921431 10.3274138,44.9429099 10.2567241,44.9815718 C9.65155172,45.3140648 8.96448276,45.4893324 8.26706897,45.4893324 Z M6.51275862,60.9738817 C4.78344828,60.9738817 3.15413793,60.3063183 1.92741379,59.0931915 C0.684310345,57.8646 -0.000172413793,56.2270507 -0.000172413793,54.482107 C-0.000172413793,52.6718676 0.653275862,50.8659239 1.94206897,49.1158254 C2.97482759,47.7128254 4.20413793,46.6388817 5.05241379,45.9859239 C5.46793103,45.6663183 5.98603448,45.4901915 6.51103448,45.4901915 C7.03862069,45.4901915 7.55931034,45.6671775 7.97741379,45.9893606 C8.19206897,46.1543183 8.40758621,46.3287268 8.61706897,46.5065718 C9.02913793,46.8553887 9.07827586,47.4731211 8.72827586,47.882938 C8.54206897,48.1020225 8.27051724,48.2266 7.98258621,48.2266 C7.74982759,48.2266 7.52396552,48.143262 7.34724138,47.993769 C7.16103448,47.8356845 6.9687931,47.6801775 6.77741379,47.5341211 C6.70241379,47.4748394 6.60758621,47.4430507 6.51189655,47.4430507 C6.41706897,47.4430507 6.32655172,47.4731211 6.25155172,47.5315437 C4.96103448,48.5255859 1.95931034,51.1915437 1.95931034,54.4812479 C1.95931034,55.7012479 2.43775862,56.8465014 3.30672414,57.7047972 C4.16448276,58.5536423 5.29982759,59.0210225 6.50241379,59.0210225 C7.82568966,59.0098535 9.00155172,58.4754592 9.86448276,57.5157831 C10.7446552,56.5346282 11.1722414,55.2209803 11.0377586,53.910769 C10.9334483,52.8978254 10.5472414,51.8616845 9.89034483,50.8324169 C9.59982759,50.3787831 9.73431034,49.7747972 10.1894828,49.485262 C10.3472414,49.3847408 10.5291379,49.3314732 10.7153448,49.3314732 C11.0524138,49.3314732 11.3618966,49.5015859 11.542931,49.7842479 C12.3653448,51.0721211 12.8506897,52.3935014 12.9877586,53.7123042 C13.1782759,55.5672197 12.5722414,57.4290085 11.3239655,58.8174028 C10.0894828,60.1920507 8.40155172,60.9584169 6.56965517,60.9738817 L6.51275862,60.9738817 Z" id="Fill-1" fill="url(#linearGradient-3)" mask="url(#mask-2)"></path>\n' +
                    '                </g>\n' +
                    '            </g>\n' +
                    '        </g>\n' +
                    '    </g>\n' +
                    '</svg>',
                title: 'Репродуктивная медицина',

            },
            {
                open: false,

                id: 3,
                imgSrc: '<?xml version="1.0" encoding="UTF-8"?>\n' +
                    '<svg width="46px" height="58px" viewBox="0 0 46 58" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">\n' +
                    '    <!-- Generator: Sketch 52.2 (67145) - http://www.bohemiancoding.com/sketch -->\n' +
                    '    <title>Group 16</title>\n' +
                    '    <desc>Created with Sketch.</desc>\n' +
                    '    <defs>\n' +
                    '        <polygon id="path-1" points="5.07132366e-05 0.119892105 45.4348294 0.119892105 45.4348294 57.2368421 5.07132366e-05 57.2368421"></polygon>\n' +
                    '        <linearGradient x1="63.9594438%" y1="100%" x2="63.9594438%" y2="37.7374922%" id="linearGradient-3">\n' +
                    '            <stop stop-color="#108BBF" offset="0%"></stop>\n' +
                    '            <stop stop-color="#82D2E3" offset="100%"></stop>\n' +
                    '        </linearGradient>\n' +
                    '    </defs>\n' +
                    '    <g id="main-landing" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\n' +
                    '        <g id="Main_Med" transform="translate(-531.000000, -643.000000)">\n' +
                    '            <g id="Medecines" transform="translate(75.000000, 585.000000)">\n' +
                    '                <g id="Group-16" transform="translate(456.000000, 58.000000)">\n' +
                    '                    <path d="M43.1894,33.7005184 C42.8513,33.7005184 42.5515333,33.5013342 42.4081667,33.1808079 C42.2096,32.7313079 42.4005,32.2337289 42.8390333,32.0414132 C42.9517333,31.9918079 43.0705667,31.9666237 43.1894,31.9666237 C43.5290333,31.9666237 43.8295667,32.1665711 43.9721667,32.4886237 C44.1699667,32.9320184 43.9813667,33.4303605 43.5428333,33.6242026 C43.4286,33.6753342 43.3097667,33.7005184 43.1894,33.7005184" id="Fill-1" fill="#6DC5DC"></path>\n' +
                    '                    <g id="Group-7" transform="translate(0.000000, 0.643571)">\n' +
                    '                        <mask id="mask-2" fill="white">\n' +
                    '                            <use xlink:href="#path-1"></use>\n' +
                    '                        </mask>\n' +
                    '                        <g id="Clip-6"></g>\n' +
                    '                        <path d="M3.51141,35.1534184 L3.11657667,34.2849447 C1.40537667,30.5118921 0.655576667,22.9032079 4.48431,18.5570237 C6.19397667,16.6163132 8.11907667,15.5845237 10.20441,15.4929447 C10.3063767,15.4883658 10.4098767,15.4860763 10.5133767,15.4860763 C13.0525767,15.4860763 15.9490433,16.8857079 18.8876767,19.5369184 L19.1176767,19.7452605 L18.97661,20.0215237 C18.23601,21.4669447 16.0226433,25.0522605 11.4816767,27.1097342 C6.76667667,29.2458132 4.76107667,32.1267342 3.90777667,34.1681816 L3.51141,35.1534184 Z M6.84487667,2.57344474 C6.84487667,2.16057632 7.18221,1.82402368 7.59697667,1.82402368 L11.0270433,1.82402368 C11.44181,1.82402368 11.7791433,2.16057632 11.7791433,2.57344474 L11.7791433,13.8544447 L11.3367767,13.8162868 C11.06461,13.7933921 10.7947433,13.7804184 10.5248767,13.7804184 C10.39301,13.7804184 10.2611433,13.7834711 10.1292767,13.7903395 C9.19164333,13.83155 8.27394333,14.0238658 7.39994333,14.3619447 L6.84487667,14.5771553 L6.84487667,2.57344474 Z M19.0049767,17.0993921 C19.0049767,13.6026026 20.1457767,10.7812079 22.3959433,8.71686579 C26.5060433,4.94228684 33.39531,4.53476053 36.16681,4.53476053 L36.8629433,4.54010263 L36.8629433,7.65760263 L36.47271,7.67439211 C33.44821,7.80412895 30.95041,8.80844474 29.0498433,10.6598658 C26.3350767,13.3042079 25.5369767,17.0299447 25.3031433,19.0149184 L25.2494767,19.4728132 L24.80021,19.3613921 C23.5060767,19.0393395 22.0616767,18.7829184 20.5068767,18.5967079 L20.37501,18.5814447 L20.27841,18.4913921 C19.9004433,18.1426289 19.5232433,17.8129447 19.1483433,17.5023395 L19.0049767,17.2894184 L19.0049767,17.0993921 Z M36.2641767,19.9703921 C36.2641767,20.4984974 36.1959433,21.0266026 36.06331,21.5432605 L35.9544433,21.9614711 L35.5427433,21.8279184 C34.4456433,21.4738132 33.30561,21.2944711 32.15561,21.2944711 C31.4901433,21.2944711 30.8223767,21.3562868 30.1714767,21.4761026 L30.0035767,21.5081553 L29.8632767,21.4104711 C29.3971433,21.0868921 28.55841,20.5961816 27.2205767,20.0978395 L26.9307767,19.9909974 L26.9568433,19.6834447 C27.0618767,18.4662079 27.42221,16.1027079 28.65271,13.9559447 L28.7508433,13.7865237 L28.9440433,13.7559974 C29.2798433,13.7041026 29.61641,13.6773921 29.9422433,13.6773921 C33.42751,13.6773921 36.2641767,16.5010763 36.2641767,19.9703921 Z M45.11611,36.4240763 C45.0501767,36.0073921 44.69521,35.7051816 44.27201,35.7051816 C44.2267767,35.7051816 44.18231,35.7089974 44.13631,35.7158658 C43.6701767,35.7883658 43.3512433,36.2264184 43.42561,36.6911816 C43.7138767,38.5097868 43.7920767,40.3619711 43.66021,42.1966026 C43.34971,46.50005 41.16471,50.4280237 37.66411,52.9723921 L36.81081,53.5943658 C35.0658767,54.8612079 33.00201,55.5312605 30.8430767,55.5312605 C30.0587767,55.5312605 29.27601,55.4412079 28.5177767,55.2626289 L25.1697433,54.4742868 C18.61321,52.9342342 12.6324433,49.31305 8.32837667,44.2769711 C6.27601,41.8753132 5.45184333,40.1719447 4.86687667,38.6898921 L4.81397667,38.5578658 C4.81397667,38.5578658 4.21674333,32.27555 12.1908433,28.6612342 C17.38731,26.3076553 19.8030767,22.2446026 20.6011767,20.6076289 L20.73151,20.3428132 L21.0243767,20.3832605 C26.8778767,21.1853395 28.94481,22.8345237 29.19091,23.0497342 C29.34041,23.2069447 29.5282433,23.2863132 29.7490433,23.2863132 C29.8195767,23.2863132 29.89471,23.2771553 29.9698433,23.2611289 L30.2887767,23.1909184 C30.8982767,23.0634711 31.52311,22.9993658 32.1479433,22.9993658 C35.8225767,22.9993658 39.17061,25.1682605 40.67711,28.5246289 C40.81511,28.8321816 41.1225433,29.0313658 41.4598767,29.0313658 C41.5794767,29.0313658 41.6975433,29.0054184 41.8079433,28.9565763 C42.23881,28.7642605 42.4335433,28.25905 42.2418767,27.8293921 C41.3180433,25.7734447 39.7356433,24.0029184 37.78601,22.8421553 L37.5544767,22.6414447 L37.61121,22.3606026 C37.8534767,21.5898132 38.0006767,20.7854447 37.9761433,19.9696289 C37.8902767,17.1024447 36.69811,15.5700237 36.1614433,14.9137079 C34.5529767,12.9501026 32.25451,12.2189974 31.0646433,12.0526289 L30.1285433,11.9228921 L30.86761,11.3344974 C32.5105767,10.0272079 34.6334767,9.36326053 37.1749767,9.36326053 C37.3451767,9.36326053 37.51691,9.36631316 37.7223767,9.37318158 C37.9431767,9.37318158 38.1547767,9.28694474 38.31501,9.13355 C38.48061,8.97328684 38.5756767,8.74968158 38.5756767,8.52073421 L38.5756767,3.72886579 C38.5756767,3.28852368 38.2306767,2.91610263 37.7898433,2.87947105 C37.6740767,2.86955 37.04311,2.82299737 36.05181,2.82299737 C34.7745433,2.82299737 31.20571,3.19770789 29.6156433,3.48312895 C26.2108767,4.09289211 23.30061,5.56883947 21.24211,7.45689211 C18.9911767,9.51818158 17.6893767,12.2121289 17.37121,15.4639447 L17.3075767,16.1126289 L16.7509767,15.7692079 C15.7481767,15.1495237 14.7438433,14.6679711 13.7686433,14.3382868 L13.4918767,14.2451816 C13.4918767,14.2451816 13.4205767,5.90386579 13.4205767,2.70852368 C13.4205767,1.04865526 12.5856767,0.119892105 11.0270433,0.119892105 L7.59697667,0.119892105 C6.23767667,0.119892105 5.13214333,1.22036579 5.13214333,2.57344474 L5.13214333,15.6310763 L4.97497667,15.7524184 C4.35244333,16.2347342 3.75291,16.8002342 3.19554333,17.4328921 C0.50991,20.4832342 -0.02369,24.4997342 0.000843333333,27.3333395 C0.0276766667,30.5370763 0.80661,33.8247605 2.06164333,35.9738132 C2.25024333,36.4774974 2.42044333,36.9628658 2.59447667,37.4589184 C3.43091,39.8460763 4.22134333,42.1012079 7.02351,45.3804974 C11.5629433,50.6913132 17.8672433,54.5109184 24.7764433,56.1349184 L28.1252433,56.9209711 C29.0153433,57.1308395 29.9299767,57.2369184 30.8430767,57.2369184 C33.36771,57.2369184 35.7811767,56.4531553 37.8189767,54.9703395 L38.67381,54.3491289 C42.58151,51.5086553 45.02181,47.12355 45.3675767,42.3187079 C45.5101767,40.3573921 45.4258433,38.3747079 45.11611,36.4240763 Z" id="Fill-5" fill="url(#linearGradient-3)" mask="url(#mask-2)"></path>\n' +
                    '                    </g>\n' +
                    '                    <path d="M26.13628,51.9038184 C26.0703467,51.9038184 26.0036467,51.8961868 25.93848,51.8816868 C21.6712133,50.8781342 17.6339467,48.8763711 14.2652133,46.0938974 C14.08888,45.9481342 13.98078,45.7436079 13.9593133,45.51695 C13.9378467,45.2902921 14.0068467,45.0682132 14.1525133,44.8926868 C14.3158133,44.6980816 14.55578,44.5851342 14.8118467,44.5851342 C15.01348,44.5851342 15.2028467,44.6530553 15.3577133,44.7820289 C18.52788,47.3981342 22.3221133,49.2793184 26.33178,50.2218184 C26.5541133,50.2737132 26.7427133,50.4095553 26.8638467,50.6033974 C26.9834467,50.7972395 27.0210133,51.0261868 26.96888,51.2482658 C26.87688,51.6336605 26.5349467,51.9038184 26.13628,51.9038184" id="Fill-10" fill="#2397C5"></path>\n' +
                    '                    <path d="M11.5374133,43.0753026 C11.2821133,43.0753026 11.0421467,42.9638816 10.87808,42.7685132 C10.56528,42.4090658 10.6059133,41.8725658 10.9624133,41.56425 C11.11728,41.4314605 11.31508,41.3574342 11.5213133,41.3574342 C11.7720133,41.3574342 12.00968,41.4658026 12.1737467,41.6543026 C12.4957467,42.0389342 12.4474467,42.5777237 12.0840467,42.8784079 C11.9307133,43.0058553 11.73598,43.0753026 11.5374133,43.0753026" id="Fill-14" fill="#39A5CC"></path>\n' +
                    '                </g>\n' +
                    '            </g>\n' +
                    '        </g>\n' +
                    '    </g>\n' +
                    '</svg>',
                title: 'Кардиология',

            },
            {
                open: false,

                id: 4,
                imgSrc: '<?xml version="1.0" encoding="UTF-8"?>\n' +
                    '<svg width="48px" height="50px" viewBox="0 0 48 50" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">\n' +
                    '    <!-- Generator: Sketch 52.2 (67145) - http://www.bohemiancoding.com/sketch -->\n' +
                    '    <title>Group 3</title>\n' +
                    '    <desc>Created with Sketch.</desc>\n' +
                    '    <defs>\n' +
                    '        <polygon id="path-1" points="0.000109841237 0 47.9999272 0 47.9999272 49.6678571 0.000109841237 49.6678571"></polygon>\n' +
                    '        <linearGradient x1="63.9594438%" y1="100%" x2="63.9594438%" y2="37.7374922%" id="linearGradient-3">\n' +
                    '            <stop stop-color="#108BBF" offset="0%"></stop>\n' +
                    '            <stop stop-color="#82D2E3" offset="100%"></stop>\n' +
                    '        </linearGradient>\n' +
                    '    </defs>\n' +
                    '    <g id="main-landing" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\n' +
                    '        <g id="Main_Med" transform="translate(-689.000000, -645.000000)">\n' +
                    '            <g id="Medecines" transform="translate(75.000000, 585.000000)">\n' +
                    '                <g id="Group-3" transform="translate(614.000000, 60.000000)">\n' +
                    '                    <mask id="mask-2" fill="white">\n' +
                    '                        <use xlink:href="#path-1"></use>\n' +
                    '                    </mask>\n' +
                    '                    <g id="Clip-2"></g>\n' +
                    '                    <path d="M22.4809455,12.4545714 C22.4809455,12.0195714 22.8416727,11.666 23.2838545,11.666 C23.7267636,11.666 24.0874909,12.0195714 24.0874909,12.4545714 C24.0874909,12.8888571 23.7267636,13.2431429 23.2838545,13.2431429 C22.8416727,13.2431429 22.4809455,12.8888571 22.4809455,12.4545714 Z M25.5689455,12.4545714 C25.5689455,11.2174286 24.5442182,10.2102857 23.2838545,10.2102857 C22.0242182,10.2102857 20.9994909,11.2174286 20.9994909,12.4545714 C20.9994909,13.691 22.0242182,14.6981429 23.2838545,14.6981429 C24.5442182,14.6981429 25.5689455,13.691 25.5689455,12.4545714 Z M18.7602182,25.5081429 C18.8773091,25.576 19.0060364,25.6081429 19.1325818,25.6081429 C19.3878545,25.6081429 19.6358545,25.4788571 19.7733091,25.2474286 C19.8620364,25.0974286 19.9398545,24.9552857 20.0154909,24.8181429 C20.4278545,24.0638571 20.5805818,23.7824286 21.5260364,23.8288571 C21.9347636,23.8495714 22.2824,23.5395714 22.3020364,23.1381429 C22.3224,22.7374286 22.0082182,22.3952857 21.5994909,22.3752857 C19.7224,22.281 19.1602182,23.306 18.7100364,24.1295714 C18.6409455,24.2552857 18.5696727,24.386 18.4940364,24.5131429 C18.2874909,24.8595714 18.4060364,25.3052857 18.7602182,25.5081429 Z M39.2249455,24.6895714 C39.5216727,24.6895714 39.8002182,24.5138571 39.9151273,24.226 C40.0642182,23.8524286 39.8751273,23.4302857 39.4947636,23.2845714 C38.6744,22.9695714 37.8162182,22.5231429 37.6525818,22.326 C37.4278545,21.9945714 36.9718545,21.9038571 36.6314909,22.1224286 C36.2889455,22.3424286 36.1929455,22.7938571 36.4169455,23.1295714 C36.8525818,23.7817143 38.3376727,24.4031429 38.9558545,24.6402857 C39.0438545,24.6731429 39.1354909,24.6895714 39.2249455,24.6895714 Z M19.1769455,35.1517143 C19.5158545,35.2838571 19.8242182,35.3374286 20.1049455,35.3374286 C20.8896727,35.3374286 21.4664,34.9167143 21.9202182,34.5852857 C22.3551273,34.2674286 22.6431273,34.0717143 23.0176727,34.0717143 L23.0365818,34.0717143 C23.4416727,34.0717143 23.7725818,33.7502857 23.7769455,33.3517143 C23.7813091,32.9495714 23.4525818,32.6202857 23.0431273,32.6167143 L23.0169455,32.6167143 C22.1325818,32.6167143 21.5253091,33.061 21.0358545,33.4181429 C20.4693091,33.8324286 20.2198545,33.9917143 19.7216727,33.7988571 C19.3420364,33.6517143 18.9114909,33.8345714 18.7609455,34.2081429 C18.6104,34.5824286 18.7973091,35.0045714 19.1769455,35.1517143 Z M13.8591273,18.0845714 C14.2656727,18.0945714 14.6082182,17.7781429 14.6191273,17.3767143 C14.6300364,16.9745714 14.3078545,16.6402857 13.8984,16.6295714 C13.0227636,16.6052857 12.4424,16.8831429 11.9274909,17.1274286 C11.4038545,17.376 10.9507636,17.5895714 10.1144,17.521 C9.70712727,17.4881429 9.34785455,17.7845714 9.3144,18.1845714 C9.27949091,18.5852857 9.58276364,18.9374286 9.99003636,18.971 C10.1398545,18.9838571 10.2831273,18.9888571 10.4191273,18.9888571 C11.4074909,18.9888571 12.0453091,18.6874286 12.5711273,18.4381429 C13.0213091,18.2245714 13.3456727,18.071 13.8591273,18.0845714 Z M13.0860364,30.2124286 C13.5282182,30.2124286 13.8889455,30.566 13.8889455,31.0017143 C13.8889455,31.436 13.5282182,31.7895714 13.0860364,31.7895714 C12.6431273,31.7895714 12.2831273,31.436 12.2831273,31.0017143 C12.2831273,30.566 12.6431273,30.2124286 13.0860364,30.2124286 Z M13.0860364,33.2445714 C14.3456727,33.2445714 15.3711273,32.2381429 15.3711273,31.0017143 C15.3711273,29.7638571 14.3456727,28.7574286 13.0860364,28.7574286 C11.8264,28.7574286 10.8009455,29.7638571 10.8009455,31.0017143 C10.8009455,32.2381429 11.8264,33.2445714 13.0860364,33.2445714 Z M31.0467636,24.0845714 C31.6365818,24.0845714 32.1158545,24.5552857 32.1158545,25.1338571 C32.1158545,25.7131429 31.6365818,26.1838571 31.0467636,26.1838571 C30.4576727,26.1838571 29.9784,25.7131429 29.9784,25.1338571 C29.9784,24.5552857 30.4576727,24.0845714 31.0467636,24.0845714 Z M31.0467636,27.6395714 C32.4533091,27.6395714 33.5973091,26.516 33.5973091,25.1345714 C33.5973091,23.7524286 32.4533091,22.6295714 31.0467636,22.6295714 C29.6409455,22.6295714 28.4962182,23.7524286 28.4962182,25.1345714 C28.4962182,26.516 29.6409455,27.6395714 31.0467636,27.6395714 Z M30.6249455,35.401 C31.2147636,35.401 31.6940364,35.8724286 31.6940364,36.451 C31.6940364,37.0295714 31.2147636,37.5002857 30.6249455,37.5002857 C30.0358545,37.5002857 29.5565818,37.0295714 29.5565818,36.451 C29.5565818,35.8717143 30.0358545,35.401 30.6249455,35.401 Z M28.0744,36.451 C28.0744,37.8317143 29.2191273,38.956 30.6249455,38.956 C32.0314909,38.956 33.1754909,37.8317143 33.1754909,36.451 C33.1754909,35.0695714 32.0314909,33.946 30.6249455,33.946 C29.2191273,33.946 28.0744,35.0695714 28.0744,36.451 Z M46.4656727,41.2731429 C46.3522182,41.5517143 46.0816727,41.7317143 45.7762182,41.7317143 C45.6831273,41.7317143 45.5907636,41.7145714 45.5027636,41.6802857 C43.8976727,41.0531429 43.1667636,39.986 42.5224,39.0438571 C42.3442182,38.7831429 42.1754909,38.5438571 42.0031273,38.3252857 C42.1864,37.7688571 42.3216727,37.1988571 42.4089455,36.616 C42.9864,37.1138571 43.3864,37.6967143 43.7529455,38.231 C44.3282182,39.0724286 44.8722182,39.8667143 46.0504,40.3267143 C46.4307636,40.476 46.6169455,40.8995714 46.4656727,41.2731429 Z M31.6693091,47.8674286 C31.5333091,48.0838571 31.2969455,48.2131429 31.0373091,48.2131429 C30.9005818,48.2131429 30.7667636,48.1752857 30.6482182,48.1038571 C29.6700364,47.5088571 28.7565818,46.4067143 28.1478545,45.1467143 C28.8002182,45.3645714 29.4656727,45.521 30.1405818,45.6138571 C30.4540364,46.0452857 30.8911273,46.5388571 31.4285818,46.8652857 C31.5973091,46.9667143 31.7144,47.1274286 31.7602182,47.3174286 C31.8053091,47.506 31.7733091,47.7017143 31.6693091,47.8674286 Z M35.1224,3.86814286 C34.5645818,3.71957143 33.9907636,3.62885714 33.4096727,3.60171429 C34.1965818,2.53671429 35.1747636,1.82457143 36.3202182,1.48671429 C36.7114909,1.37028571 37.1274909,1.58885714 37.2445818,1.97314286 C37.3624,2.35742857 37.1398545,2.76528571 36.7485818,2.881 C36.1493091,3.05814286 35.6045818,3.38957143 35.1224,3.86814286 Z M14.1784,9.21671429 L13.6918545,9.31528571 C13.3165818,8.95885714 12.7253091,8.476 11.8082182,7.88457143 C10.4518545,7.00957143 9.13185455,6.38671429 8.5784,6.24528571 C8.18276364,6.14385714 7.94494545,5.74528571 8.04749091,5.35671429 C8.15076364,4.96885714 8.55730909,4.73457143 8.95221818,4.836 C9.84385455,5.06385714 11.5820364,5.95671429 12.9936727,6.91171429 C13.6794909,7.376 14.2474909,7.821 14.6918545,8.23957143 L14.1784,9.21671429 Z M5.30421818,24.621 L4.96749091,25.8888571 C3.99076364,26.3738571 3.19730909,27.1888571 2.78785455,27.6645714 C2.64676364,27.8274286 2.44021818,27.9217143 2.22130909,27.9217143 C2.09767273,27.9217143 1.9144,27.8917143 1.74276364,27.7495714 C1.43076364,27.4902857 1.39149091,27.0295714 1.65549091,26.7238571 C2.68603636,25.5267143 3.82058182,24.6988571 4.95803636,24.3038571 L5.30421818,24.621 Z M8.4744,41.0774286 C8.93985455,41.4452857 9.43003636,41.7767143 9.94203636,42.071 C8.68967273,42.5081429 7.52530909,43.056 7.12676364,44.2545714 C7.02712727,44.5545714 6.74349091,44.7567143 6.42203636,44.7567143 C6.34494545,44.7567143 6.26712727,44.7445714 6.19221818,44.7202857 C5.80312727,44.596 5.59003636,44.1838571 5.71658182,43.8024286 C6.21185455,42.3131429 7.39076364,41.5552857 8.4744,41.0774286 Z M41.0387636,11.9988571 C42.2649455,11.5167143 43.5434909,10.7295714 44.3536727,8.97385714 C44.5224,8.60742857 44.9624,8.44528571 45.3347636,8.61028571 C45.5151273,8.69028571 45.6533091,8.83528571 45.7231273,9.01742857 C45.7929455,9.19885714 45.7864,9.39742857 45.7049455,9.57457143 C44.5245818,12.1338571 42.5260364,13.0502857 40.9580364,13.5802857 L41.0387636,11.9988571 Z M46.5987636,38.9745714 C45.8460364,38.681 45.5020364,38.1788571 44.9820364,37.4188571 C44.4598545,36.656 43.7609455,35.6395714 42.5245818,34.9024286 C42.5202182,34.6152857 42.5049455,34.3267143 42.4780364,34.0374286 L42.3827636,33.0138571 L45.8642182,28.9067143 C47.7391273,26.6952857 48.2380364,23.7681429 47.1965818,21.0781429 C46.1565818,18.3881429 43.8045818,16.5238571 40.9078545,16.0938571 L40.8307636,16.0817143 L40.8787636,15.1388571 C42.5522182,14.6117143 45.4714909,13.6052857 47.0540364,10.1752857 C47.2991273,9.64314286 47.3187636,9.05028571 47.1093091,8.50457143 C46.8998545,7.95885714 46.4874909,7.526 45.9464,7.28528571 C44.8293091,6.78885714 43.5100364,7.27671429 43.0045818,8.37314286 C42.5209455,9.42242857 41.8409455,10.0124286 40.9864,10.4295714 C40.7718545,8.79171429 40.0416727,7.27314286 38.8605818,6.05314286 C38.2351273,5.40742857 37.5056727,4.87814286 36.7122182,4.476 C36.8620364,4.38957143 37.0169455,4.321 37.1762182,4.27385714 C38.3493091,3.92742857 39.0169455,2.70671429 38.6634909,1.55385714 C38.3107636,0.401714286 37.0685818,-0.254 35.8940364,0.0924285714 C34.3260364,0.556714286 33.0184,1.546 32.0089455,3.03314286 C31.8591273,3.25314286 31.7158545,3.486 31.5805818,3.72742857 C30.7864,3.87457143 30.0118545,4.14028571 29.2911273,4.516 L28.4613091,4.94814286 C26.9500364,3.361 24.8307636,2.45457143 22.6169455,2.45457143 C19.6009455,2.45457143 16.8700364,4.08814286 15.4904,6.71814286 L15.3965818,6.89742857 C14.9565818,6.51528571 14.4394909,6.12314286 13.8344,5.71314286 C12.2496727,4.64171429 10.3958545,3.70171429 9.32603636,3.42742857 C8.13985455,3.12385714 6.92312727,3.82528571 6.61476364,4.98957143 C6.30494545,6.15457143 7.01767273,7.34957143 8.20458182,7.65314286 C8.51076364,7.73171429 9.67949091,8.25242857 10.9951273,9.101 C11.3311273,9.31671429 11.6133091,9.51385714 11.8511273,9.691 L8.32094545,10.4095714 C5.37912727,11.0081429 3.04676364,13.1445714 2.2344,15.9852857 C1.50785455,18.5267143 2.11876364,21.2081429 3.82712727,23.1888571 C2.66494545,23.7238571 1.54058182,24.6045714 0.525309091,25.7831429 C-0.266690909,26.7024286 -0.1496,28.0831429 0.7864,28.861 C1.18712727,29.1938571 1.69694545,29.3767143 2.22130909,29.3767143 C2.87730909,29.3767143 3.49621818,29.0952857 3.91949091,28.6038571 C4.06785455,28.431 4.22130909,28.2695714 4.37621818,28.1181429 L4.24894545,28.5988571 C3.2824,32.2395714 4.07149091,36.0352857 6.41258182,39.011 C6.69185455,39.3667143 6.98858182,39.7031429 7.30058182,40.0217143 C6.13549091,40.6281429 4.88749091,41.6095714 4.30858182,43.3502857 C3.92821818,44.4945714 4.56676364,45.7302857 5.73185455,46.1031429 C5.95585455,46.1752857 6.18858182,46.2117143 6.42203636,46.2117143 C7.3864,46.2117143 8.23585455,45.6067143 8.53476364,44.7052857 C8.73912727,44.0895714 9.4904,43.7495714 11.0954909,43.2202857 C11.3216727,43.146 11.5900364,43.0574286 11.8613091,42.9602857 C12.7900364,43.2967143 13.7667636,43.5224286 14.7762182,43.6302857 C15.1834909,43.6731429 15.5485818,43.3845714 15.5922182,42.9845714 C15.6365818,42.5852857 15.3427636,42.2267143 14.9354909,42.1831429 C13.9791273,42.0817143 13.0554909,41.8581429 12.1827636,41.5231429 C12.1529455,41.5095714 12.1224,41.4974286 12.0911273,41.4874286 C11.0074909,41.061 10.0024,40.4602857 9.10858182,39.7031429 C9.09476364,39.6902857 9.07949091,39.6767143 9.06421818,39.6652857 C8.52749091,39.2052857 8.03221818,38.6902857 7.58494545,38.1217143 C5.52603636,35.5052857 4.83221818,32.1681429 5.6824,28.9652857 L6.77403636,24.8502857 C6.88894545,24.4188571 6.7544,23.961 6.42203636,23.656 L5.48676364,22.7967143 C3.6984,21.1531429 2.99876364,18.6938571 3.66058182,16.3788571 C4.32312727,14.0638571 6.22349091,12.3217143 8.62130909,11.8345714 L14.5929455,10.6188571 C14.9609455,10.5438571 15.2707636,10.3124286 15.4424,9.98528571 L16.8067636,7.38457143 C17.9318545,5.241 20.1587636,3.90957143 22.6169455,3.90957143 C24.4722182,3.90957143 26.2467636,4.69171429 27.4845818,6.056 C27.7194909,6.31385714 28.0562182,6.46242857 28.4096727,6.46242857 C28.6133091,6.46242857 28.8074909,6.41457143 28.9878545,6.32028571 L29.9849455,5.80171429 C30.6649455,5.44671429 31.4016727,5.21385714 32.1529455,5.11028571 C32.1558545,5.10957143 32.1587636,5.10957143 32.1616727,5.10885714 C32.4533091,5.06885714 32.7478545,5.04814286 33.0416727,5.04814286 C33.7478545,5.04814286 34.4453091,5.16457143 35.1078545,5.38385714 C35.1129455,5.38528571 35.1187636,5.38671429 35.1238545,5.38814286 C36.1253091,5.72171429 37.0453091,6.29028571 37.7871273,7.056 C39.0242182,8.33242857 39.6554909,10.0124286 39.5653091,11.7852857 L39.3405818,16.226 C39.3085818,16.8517143 39.7602182,17.3945714 40.3914909,17.4881429 L40.6860364,17.5317143 C43.0474909,17.8838571 44.9638545,19.4024286 45.8118545,21.5952857 C46.6605818,23.7874286 46.2540364,26.1731429 44.7260364,27.976 L41.1791273,32.1595714 C40.9711273,32.406 40.8678545,32.7295714 40.8976727,33.0474286 L41.0031273,34.1702857 C41.2474909,36.7902857 40.3944,39.296 38.6009455,41.226 C36.8082182,43.156 34.2714909,44.2624286 31.6409455,44.2624286 C31.3114909,44.2624286 30.9820364,44.2452857 30.6562182,44.211 L30.6547636,44.211 C29.6962182,44.1102857 28.7558545,43.8638571 27.8504,43.4731429 L26.0111273,42.6781429 C24.7820364,42.1481429 23.4780364,41.8795714 22.1362182,41.8795714 C21.9129455,41.8795714 21.6860364,41.8874286 21.4620364,41.9031429 L18.2853091,42.1217143 C17.8773091,42.1502857 17.5696727,42.4981429 17.5980364,42.8988571 C17.6271273,43.3002857 17.9813091,43.6017143 18.3893091,43.5731429 L21.5653091,43.3545714 C22.8976727,43.2631429 24.2016727,43.4874286 25.4147636,44.011 L26.2649455,44.3774286 C26.8918545,46.4545714 28.2802182,48.376 29.8700364,49.3417143 C30.2220364,49.5545714 30.6256727,49.6681429 31.0380364,49.6681429 C31.8140364,49.6681429 32.5216727,49.281 32.9296727,48.6324286 C33.2424,48.136 33.3391273,47.551 33.2024,46.9831429 C33.0758545,46.4552857 32.7602182,46.0038571 32.3107636,45.696 C35.1136727,45.5238571 37.7747636,44.2745714 39.6954909,42.2074286 C40.3500364,41.5024286 40.8969455,40.731 41.3296727,39.9102857 C42.0147636,40.911 42.9602182,42.2531429 44.9551273,43.0331429 C45.2191273,43.1352857 45.4954909,43.1874286 45.7762182,43.1874286 C46.6925818,43.1874286 47.5034909,42.6474286 47.8424,41.811 C48.2954909,40.6924286 47.7376727,39.4202857 46.5987636,38.9745714 Z M32.1507636,14.226 C32.2438545,14.2638571 32.3398545,14.2824286 32.4351273,14.2824286 C32.7253091,14.2824286 33.0009455,14.1131429 33.1194909,13.8338571 C33.2765818,13.4624286 33.0969455,13.0367143 32.7194909,12.8824286 C32.1929455,12.6681429 31.4220364,11.576 31.3624,11.2431429 C31.3224,10.8452857 30.9645818,10.5531429 30.5573091,10.5902857 C30.1507636,10.626 29.8504,10.981 29.8882182,11.3802857 C29.9653091,12.216 31.1202182,13.806 32.1507636,14.226 Z M26.7769455,40.966 L28.6162182,41.7595714 C29.5965818,42.1824286 30.6264,42.3917143 31.6489455,42.3917143 C33.1885818,42.3917143 34.7093091,41.9188571 36.0184,40.9931429 C38.1965818,39.4524286 39.3500364,36.9652857 39.1049455,34.341 L39.0009455,33.2188571 C38.9238545,32.3974286 39.1776727,31.596 39.7151273,30.9617143 L43.2620364,26.7788571 C44.3442182,25.5024286 44.6322182,23.8124286 44.0314909,22.2595714 C43.4307636,20.7067143 42.0729455,19.631 40.4009455,19.3817143 L40.1056727,19.3381429 C39.1682182,19.1988571 38.3529455,18.6588571 37.8693091,17.856 C37.6613091,17.5102857 37.2067636,17.3952857 36.8554909,17.6002857 C36.5027636,17.8045714 36.3864,18.2502857 36.5944,18.5967143 C37.3056727,19.7774286 38.5042182,20.5717143 39.8838545,20.7767143 L40.1791273,20.821 C41.3151273,20.9895714 42.2380364,21.721 42.6467636,22.776 C43.0540364,23.8317143 42.8591273,24.9795714 42.1238545,25.8474286 L38.5769455,30.0302857 C37.7856727,30.9631429 37.4125818,32.1424286 37.5253091,33.3517143 L37.6300364,34.4731429 C37.8271273,36.5795714 36.9005818,38.5752857 35.1529455,39.8124286 C33.4053091,41.0481429 31.1842182,41.2788571 29.2118545,40.4274286 L27.3733091,39.6338571 C25.4474909,38.8038571 23.3209455,38.441 21.2234909,38.5852857 L16.6489455,38.9002857 C14.2402182,39.0674286 11.9725818,38.1152857 10.4293091,36.2895714 C8.88603636,34.4638571 8.35003636,32.1002857 8.95876364,29.8045714 L10.0511273,25.6888571 C10.4794909,24.076 9.97549091,22.366 8.73549091,21.2267143 L7.79949091,20.3667143 C6.92530909,19.5645714 6.59730909,18.4095714 6.92021818,17.2788571 C7.24458182,16.1474286 8.13694545,15.3288571 9.30858182,15.0902857 L15.2809455,13.8752857 C16.6533091,13.596 17.8096727,12.7331429 18.4518545,11.5088571 L19.8169455,8.90814286 C20.2984,7.991 21.1725818,7.39028571 22.2154909,7.261 C23.2576727,7.131 24.2569455,7.49814286 24.9558545,8.26885714 C26.3769455,9.83457143 28.6882182,10.2417143 30.5747636,9.25885714 L31.5718545,8.73957143 C32.5834909,8.21314286 33.7645818,8.25814286 34.7311273,8.86171429 C35.6976727,9.46528571 36.2402182,10.4967143 36.1834909,11.6202857 L36.0169455,14.8888571 C35.9965818,15.2902857 36.3114909,15.6317143 36.7194909,15.6524286 C37.1311273,15.6724286 37.4758545,15.3624286 37.4962182,14.9617143 L37.6627636,11.6924286 C37.7464,10.0395714 36.9478545,8.52242857 35.5260364,7.63385714 C34.1034909,6.746 32.3667636,6.67885714 30.8773091,7.45385714 L29.8809455,7.97385714 C28.5980364,8.64171429 27.0278545,8.36457143 26.0620364,7.30028571 C25.0482182,6.18385714 23.5405818,5.62885714 22.0285818,5.81671429 C20.5173091,6.00528571 19.1980364,6.911 18.4998545,8.24171429 L17.1347636,10.8424286 C16.6991273,11.6738571 15.9136727,12.261 14.9798545,12.4502857 L9.00821818,13.666 C7.31003636,14.0117143 5.96385455,15.2452857 5.49476364,16.8852857 C5.02567273,18.5245714 5.52167273,20.2667143 6.78785455,21.4295714 L7.72385455,22.2902857 C8.56603636,23.0638571 8.90858182,24.2252857 8.61767273,25.3224286 L7.52603636,29.4374286 C6.79512727,32.1931429 7.43730909,35.0295714 9.28894545,37.2195714 C11.1413091,39.4095714 13.8620364,40.5524286 16.7536727,40.3524286 L21.3282182,40.0367143 C23.1864,39.9088571 25.0714909,40.2302857 26.7769455,40.966 Z" id="Fill-1" fill="url(#linearGradient-3)" mask="url(#mask-2)"></path>\n' +
                    '                </g>\n' +
                    '            </g>\n' +
                    '        </g>\n' +
                    '    </g>\n' +
                    '</svg>',
                title: 'Лечение рака',

            },
            {
                open: false,
                id: 5,
                imgSrc: '<?xml version="1.0" encoding="UTF-8"?>\n' +
                    '<svg width="52px" height="27px" viewBox="0 0 52 27" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">\n' +
                    '    <!-- Generator: Sketch 52.2 (67145) - http://www.bohemiancoding.com/sketch -->\n' +
                    '    <title>Group 3</title>\n' +
                    '    <desc>Created with Sketch.</desc>\n' +
                    '    <defs>\n' +
                    '        <polygon id="path-1" points="8e-05 0.000158823529 51.6792 0.000158823529 51.6792 26.9309912 8e-05 26.9309912"></polygon>\n' +
                    '        <linearGradient x1="63.9594438%" y1="100%" x2="63.9594438%" y2="37.7374922%" id="linearGradient-3">\n' +
                    '            <stop stop-color="#108BBF" offset="0%"></stop>\n' +
                    '            <stop stop-color="#82D2E3" offset="100%"></stop>\n' +
                    '        </linearGradient>\n' +
                    '    </defs>\n' +
                    '    <g id="main-landing" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\n' +
                    '        <g id="Main_Med" transform="translate(-853.000000, -656.000000)">\n' +
                    '            <g id="Medecines" transform="translate(75.000000, 585.000000)">\n' +
                    '                <g id="Group-3" transform="translate(778.000000, 71.000000)">\n' +
                    '                    <mask id="mask-2" fill="white">\n' +
                    '                        <use xlink:href="#path-1"></use>\n' +
                    '                    </mask>\n' +
                    '                    <g id="Clip-2"></g>\n' +
                    '                    <path d="M29.92848,6.6771 C29.77088,6.6771 29.61168,6.63898235 29.47088,6.56512941 C29.23648,6.44362941 29.06368,6.23874706 28.98368,5.9886 C28.90528,5.73765882 28.92928,5.47162941 29.05168,5.23895294 L29.58208,4.23121765 C29.75248,3.90562941 30.08928,3.70312941 30.46048,3.70312941 C30.61888,3.70312941 30.77728,3.74204118 30.91728,3.8151 C31.40128,4.06524706 31.58928,4.66083529 31.33728,5.14127647 L30.80688,6.14901176 C30.63568,6.4746 30.29888,6.6771 29.92848,6.6771 Z M21.13728,7.02730588 C20.79328,7.02730588 20.47888,6.85577647 20.29808,6.56751176 L19.68768,5.60504118 C19.54608,5.38348235 19.50128,5.11983529 19.55968,4.86412941 C19.61888,4.60842353 19.77488,4.39004118 19.99808,4.25027647 C20.15648,4.15101176 20.33888,4.0986 20.52528,4.0986 C20.86848,4.0986 21.18128,4.27092353 21.36368,4.55918824 L21.97408,5.52086471 C22.11488,5.74321765 22.16048,6.00686471 22.10208,6.26257059 C22.04288,6.51827647 21.88688,6.73665882 21.66368,6.87642353 C21.50528,6.97489412 21.32288,7.02730588 21.13728,7.02730588 Z M16.17408,16.9085118 C15.72208,16.9085118 15.32768,16.6043647 15.21488,16.1699824 C15.07968,15.6442765 15.39968,15.1074529 15.92848,14.9732471 L17.03888,14.6905412 C17.11888,14.6698941 17.20208,14.6595706 17.28368,14.6595706 C17.73648,14.6595706 18.13008,14.9637176 18.24368,15.3988941 C18.30928,15.6530118 18.27088,15.9174529 18.13648,16.1429824 C18.00128,16.3701 17.78528,16.5297176 17.52928,16.5948353 L16.41968,16.8775412 C16.34048,16.8981882 16.25728,16.9085118 16.17408,16.9085118 Z M35.38368,17.3373353 C35.28688,17.3373353 35.19008,17.3230412 35.09568,17.2944529 L33.99968,16.9625118 C33.74608,16.8862765 33.53888,16.7163353 33.41408,16.4844529 C33.28928,16.2525706 33.26368,15.9865412 33.34128,15.7356 C33.46928,15.3186882 33.85008,15.0391588 34.28848,15.0391588 C34.38608,15.0391588 34.48288,15.0534529 34.57728,15.0820412 L35.67328,15.4139824 C36.19408,15.5720118 36.49008,16.1223353 36.33168,16.6401 C36.20288,17.0578059 35.82208,17.3373353 35.38368,17.3373353 Z M31.87888,8.28915882 C30.82128,8.28915882 29.96048,9.14362941 29.96048,10.1942471 C29.96048,11.2440706 30.82128,12.0993353 31.87888,12.0993353 C32.93808,12.0993353 33.79888,11.2440706 33.79888,10.1942471 C33.79888,9.14362941 32.93808,8.28915882 31.87888,8.28915882 Z M25.83968,8.33283529 C22.98848,8.33283529 20.66928,10.6349824 20.66928,13.4652176 C20.66928,16.2954529 22.98848,18.5983941 25.83968,18.5983941 C28.51168,18.5983941 30.73328,16.5773647 30.98528,13.9615412 C29.23168,13.5533647 27.97968,12.0024529 27.97968,10.1942471 C27.97968,9.74954118 28.05488,9.31515882 28.20448,8.89983529 C27.47808,8.52898235 26.66528,8.33283529 25.83968,8.33283529 Z M25.83968,20.5638353 C21.89648,20.5638353 18.68848,17.3794235 18.68848,13.4652176 C18.68848,9.55180588 21.89648,6.36739412 25.83968,6.36739412 C27.06528,6.36739412 28.26768,6.67868824 29.32848,7.27030588 C30.03888,6.65804118 30.93728,6.32371765 31.87888,6.32371765 C34.02928,6.32371765 35.77808,8.05965882 35.77808,10.1942471 C35.77808,11.9246294 34.63488,13.4271 32.97568,13.9083353 C32.74368,17.6549824 29.64288,20.5638353 25.83968,20.5638353 Z M33.57968,20.6980412 C33.35328,20.6980412 33.13168,20.6202176 32.95568,20.4772765 L32.06688,19.7601882 C31.64368,19.4179235 31.57888,18.7985118 31.92368,18.3776294 C32.11248,18.1473353 32.39248,18.0147176 32.69248,18.0147176 C32.91808,18.0147176 33.14048,18.0933353 33.31648,18.2354824 L34.20608,18.9525706 C34.62848,19.2948353 34.69248,19.9150412 34.34928,20.3351294 C34.15968,20.5662176 33.87968,20.6980412 33.57968,20.6980412 Z M24.83248,24.3073059 C24.28608,24.2572765 23.88528,23.7776294 23.93568,23.2376294 L24.04128,22.1052176 C24.08768,21.5969824 24.51168,21.2134235 25.02688,21.2134235 L25.11728,21.2173941 C25.38048,21.2412176 25.61888,21.3658941 25.78848,21.5676 C25.95728,21.7693059 26.03728,22.0250118 26.01248,22.2862765 L25.90768,23.4186882 C25.85968,23.9277176 25.43648,24.3112765 24.92208,24.3112765 L24.83248,24.3073059 Z M25.83968,1.9656 C19.45088,1.9656 14.25408,7.12418824 14.25408,13.4652176 C14.25408,19.8062471 19.45088,24.9656294 25.83968,24.9656294 C32.22768,24.9656294 37.42528,19.8062471 37.42528,13.4652176 C37.42528,7.12418824 32.22768,1.9656 25.83968,1.9656 Z M25.83968,26.9310706 C20.18048,26.9310706 14.24768,25.1427176 8.68128,21.7589824 C8.21488,21.4754824 8.06848,20.8695706 8.35328,20.4073941 C8.53568,20.1127765 8.85168,19.9372765 9.19888,19.9372765 C9.38048,19.9372765 9.55968,19.9881 9.71568,20.0826 C11.97328,21.4548353 14.29888,22.5546882 16.64688,23.3607176 C13.85328,20.8116 12.27408,17.2682471 12.27408,13.4652176 C12.27408,9.65265882 13.85968,6.10454118 16.66448,3.55462941 C15.00208,4.12718824 13.33408,4.85539412 11.69008,5.72574706 C7.06368,8.17798235 3.83248,11.1066882 2.31728,12.6345706 C1.86288,13.0927765 1.86288,13.8384529 2.31728,14.2966588 C3.23728,15.2241882 4.77408,16.6520118 6.87088,18.1830706 C7.08368,18.3387176 7.22288,18.5674235 7.26288,18.8271 C7.30288,19.0859824 7.23808,19.3464529 7.08048,19.5576882 C6.89568,19.8078353 6.59728,19.9579235 6.28288,19.9579235 C6.07088,19.9579235 5.86848,19.8920118 5.69728,19.7665412 C3.49648,18.1592471 1.87728,16.6543941 0.90608,15.6752471 C-0.30192,14.4570706 -0.30192,12.4741588 0.90608,11.2559824 C2.50048,9.6471 5.90128,6.56433529 10.75728,3.99218824 C15.75648,1.34301176 20.83088,0.000158823529 25.83968,0.000158823529 C35.51648,0.000158823529 43.42288,5.10395294 47.27488,8.14621765 C47.70208,8.48292353 47.77328,9.10312941 47.43248,9.52718824 C47.24448,9.76304118 46.96128,9.89804118 46.65728,9.89804118 C46.43488,9.89804118 46.21568,9.82180588 46.04208,9.68442353 C43.60688,7.76107059 39.78528,5.20798235 35.03008,3.56892353 C37.82528,6.11724706 39.40528,9.6606 39.40528,13.4652176 C39.40528,17.2785706 37.81888,20.8266882 35.01408,23.3766 C36.67648,22.8048353 38.34528,22.0766294 39.98848,21.2046882 C44.61408,18.7540412 47.84688,15.8245412 49.36208,14.2966588 C49.81568,13.8384529 49.81568,13.0927765 49.36208,12.6345706 C49.12528,12.3955412 48.87728,12.1533353 48.62368,11.9135118 C48.22848,11.5394824 48.21328,10.9153059 48.59008,10.5238059 C48.77808,10.3276588 49.03328,10.2188647 49.30688,10.2188647 C49.56208,10.2188647 49.80528,10.3157471 49.99008,10.4904529 C50.25808,10.7453647 50.52208,11.0018647 50.77408,11.2559824 C51.98128,12.4741588 51.98128,14.4570706 50.77328,15.6752471 C49.17808,17.2833353 45.77808,20.3661 40.92128,22.9398353 C35.92208,25.5882176 30.84768,26.9310706 25.83968,26.9310706 Z" id="Fill-1" fill="url(#linearGradient-3)" mask="url(#mask-2)"></path>\n' +
                    '                </g>\n' +
                    '            </g>\n' +
                    '        </g>\n' +
                    '    </g>\n' +
                    '</svg>',
                title: 'Офтальмология',

            },
            {
                open: false,
                id: 6,
                imgSrc: '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="40" height="63" viewBox="0 0 40 63">\n' +
                    '    <defs>\n' +
                    '        <path id="a" d="M0 0h39.952v62.993H0z"/>\n' +
                    '        <linearGradient id="b" x1="63.959%" x2="63.959%" y1="100%" y2="37.737%">\n' +
                    '            <stop offset="0%" stop-color="#108BBF"/>\n' +
                    '            <stop offset="100%" stop-color="#82D2E3"/>\n' +
                    '        </linearGradient>\n' +
                    '    </defs>\n' +
                    '    <g fill="none" fill-rule="evenodd">\n' +
                    '        <g>\n' +
                    '            <mask id="c" fill="#fff">\n' +
                    '                <use xlink:href="#a"/>\n' +
                    '            </mask>\n' +
                    '            <path fill="url(#b)" d="M26.089 47.466a.868.868 0 0 1-.872-.863c0-.476.388-.863.864-.863.486 0 .876.387.876.863s-.39.863-.868.863M16.018 7.83c.374 0 .728.179.948.478.011.015 1.274 1.666 3.907 3.115 1.898 1.044 5.11 2.289 9.631 2.288.684 0 1.387-.028 2.093-.085-.302-3.04-1.715-5.856-3.997-7.952a12.712 12.712 0 0 0-8.624-3.354c-3.21 0-6.275 1.193-8.63 3.36a12.454 12.454 0 0 0-3.986 7.902c.593-.092 1.396-.26 2.262-.567 2.466-.875 4.265-2.404 5.347-4.544a1.18 1.18 0 0 1 1.049-.641zm10.08 23.55c4.484-1.342 7.8-3.822 9.174-4.974-.866-1.45-2.614-4.95-2.614-9.586v-.87c-.54.04-1.081.066-1.612.077v4.707c0 3.677-1.848 7.075-4.949 9.117v1.53zM4.68 26.406c1.374 1.152 4.69 3.632 9.175 4.974v-1.53c-3.1-2.04-4.948-5.439-4.948-9.116v-2.936c0-.64.526-1.16 1.173-1.16.646 0 1.173.52 1.173 1.16v2.936c0 4.757 3.913 8.628 8.723 8.628 4.81 0 8.724-3.87 8.724-8.628v-4.763c-4.188-.288-7.237-1.555-9.068-2.576a17.277 17.277 0 0 1-3.41-2.474c-2.684 3.823-6.968 4.779-8.928 5.017v.881c0 4.637-1.747 8.137-2.614 9.587zm18.81 16.658c1.976-1.196 3.207-3.34 3.23-5.626l-.458-.194a4.09 4.09 0 0 1-2.51-3.765v-2.454c-1.21.436-2.478.656-3.776.656-1.298 0-2.566-.22-3.775-.656v2.454a4.09 4.09 0 0 1-2.51 3.765l-.458.194c.023 2.285 1.254 4.43 3.23 5.626h7.027zM8.284 44.6a8.167 8.167 0 0 1 4.662-1.537 8.986 8.986 0 0 1-1.998-4.658l-2.664 1.13V44.6zm18.723-1.537a8.167 8.167 0 0 1 4.662 1.537v-5.066l-2.664-1.129a8.985 8.985 0 0 1-1.998 4.658zm-4.864 2.32c.096 1.974 1.75 3.552 3.771 3.552 1.735 0 3.285-1.231 3.678-2.89a5.797 5.797 0 0 0-2.688-.663h-4.761zm-8.105 9.424a4.138 4.138 0 0 1 3.533 1.958h4.813c.043-.07.088-.138.136-.204-2.386-1.561-4.297-3.654-5.683-6.226a1.155 1.155 0 0 1 .483-1.569c.17-.09.36-.136.551-.136.435 0 .832.235 1.036.614 1.331 2.47 3.215 4.423 5.6 5.807a4.151 4.151 0 0 1 1.407-.244h5.334a5.637 5.637 0 0 0 .258-7.143 6.087 6.087 0 0 1-1.446 1.99 6.141 6.141 0 0 1-4.146 1.6c-3.314 0-6.02-2.617-6.118-5.872h-2.372c.065.27.139.542.22.812a1.162 1.162 0 0 1-.79 1.442 1.178 1.178 0 0 1-1.458-.782c-.15-.498-.276-.992-.374-1.472h-1.973c-3.173 0-5.755 2.553-5.755 5.691 0 1.378.5 2.7 1.413 3.736l5.331-.002zM4.634 41.082a3.729 3.729 0 0 0-2.288 3.432v13.411c0 1.52 1.25 2.755 2.785 2.755h8.907c.48 0 .93-.185 1.27-.52.34-.337.526-.782.526-1.256 0-.475-.186-.92-.526-1.256-.34-.336-.79-.52-1.27-.52l-5.407.001a2.16 2.16 0 0 1-1.597-.689 7.934 7.934 0 0 1-1.096-9.197V40.53l-1.304.552zm21.28 16.046c-.99 0-1.796.797-1.796 1.776 0 .98.806 1.776 1.796 1.776h8.907c1.536 0 2.786-1.235 2.786-2.755v-13.41a3.73 3.73 0 0 0-2.288-3.433l-1.305-.552v6.713a7.934 7.934 0 0 1-1.094 9.196c-.4.438-.982.69-1.597.69l-5.409-.001zm0 5.872c-2.223 0-4.042-1.74-4.138-3.915h-3.6C18.08 61.26 16.261 63 14.038 63H5.131C2.302 63 0 60.724 0 57.925V44.514a6.045 6.045 0 0 1 3.71-5.564l9.056-3.838c.565-.24.958-.736 1.061-1.325-6.896-1.858-11.318-6.176-11.504-6.36a1.156 1.156 0 0 1-.11-1.516c.027-.036 2.735-3.729 2.735-9.092v-1.957C4.948 6.667 11.69 0 19.976 0c8.287 0 15.028 6.667 15.028 14.862v1.957c0 5.374 2.709 9.057 2.736 9.094.345.46.298 1.11-.11 1.514-.187.184-4.61 4.503-11.505 6.36a1.768 1.768 0 0 0 1.061 1.325l9.056 3.838a6.044 6.044 0 0 1 3.71 5.564v13.411c0 2.799-2.302 5.075-5.131 5.075h-8.907z" mask="url(#c)"/>\n' +
                    '        </g>\n' +
                    '        <path fill="#82D2E3" d="M15.652 19.85c-.974 0-1.868-.378-2.392-1.01a.89.89 0 0 1-.201-.744.984.984 0 0 1 .465-.649c.197-.122.429-.187.67-.187.37 0 .72.156.937.416.05.062.226.179.521.179.296 0 .471-.117.523-.179.215-.26.565-.416.936-.416.241 0 .473.065.67.187.516.321.634.946.264 1.392-.524.633-1.419 1.01-2.393 1.01M24.348 19.85c-.975 0-1.869-.378-2.393-1.01a.89.89 0 0 1-.2-.744.983.983 0 0 1 .464-.648c.197-.123.43-.188.67-.188.371 0 .72.156.937.416.051.062.226.179.522.179.296 0 .47-.117.522-.179.215-.26.565-.416.936-.416.241 0 .473.065.67.188.25.155.415.385.465.648a.89.89 0 0 1-.2.743c-.525.633-1.42 1.01-2.393 1.01"/>\n' +
                    '    </g>\n' +
                    '</svg>',
                title: 'Роды в США',
            },
            {
                open: false,
                id: 7,
                imgSrc: '<?xml version="1.0" encoding="UTF-8"?>\n' +
                    '<svg width="53px" height="53px" viewBox="0 0 53 53" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">\n' +
                    '    <!-- Generator: Sketch 52.2 (67145) - http://www.bohemiancoding.com/sketch -->\n' +
                    '    <title>Group 3</title>\n' +
                    '    <desc>Created with Sketch.</desc>\n' +
                    '    <defs>\n' +
                    '        <polygon id="path-1" points="0.000270122066 -8.03030303e-05 52.8393939 -8.03030303e-05 52.8393939 52.5059758 0.000270122066 52.5059758"></polygon>\n' +
                    '        <linearGradient x1="63.9594438%" y1="100%" x2="63.9594438%" y2="37.7374922%" id="linearGradient-3">\n' +
                    '            <stop stop-color="#108BBF" offset="0%"></stop>\n' +
                    '            <stop stop-color="#82D2E3" offset="100%"></stop>\n' +
                    '        </linearGradient>\n' +
                    '    </defs>\n' +
                    '    <g id="main-landing" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\n' +
                    '        <g id="Main_Med" transform="translate(-1208.000000, -643.000000)">\n' +
                    '            <g id="Medecines" transform="translate(75.000000, 585.000000)">\n' +
                    '                <g id="Group-3" transform="translate(1133.000000, 58.000000)">\n' +
                    '                    <mask id="mask-2" fill="white">\n' +
                    '                        <use xlink:href="#path-1"></use>\n' +
                    '                    </mask>\n' +
                    '                    <g id="Clip-2"></g>\n' +
                    '                    <path d="M40.1921485,30.2548894 C40.6908303,30.5977833 41.1204515,31.0274045 41.4633455,31.5260864 C41.8070424,31.0274045 42.2366636,30.5977833 42.7353455,30.2548894 C42.2366636,29.9119955 41.8070424,29.4815712 41.4633455,28.9836924 C41.1204515,29.4815712 40.6908303,29.9119955 40.1921485,30.2548894 Z M41.4633455,34.2339045 C40.8024515,34.2339045 40.204997,33.8428288 39.9408,33.2365409 C39.6557242,32.5812682 39.1377697,32.0633136 38.482497,31.7774348 C37.8762091,31.5140409 37.4835273,30.9165864 37.4835273,30.2548894 C37.4835273,29.5931924 37.8762091,28.9957379 38.4833,28.7315409 C39.1377697,28.4456621 39.6557242,27.9285106 39.939997,27.2740409 C40.2041939,26.667753 40.8024515,26.2758742 41.4633455,26.2758742 C42.1250424,26.2758742 42.722497,26.66695 42.9866939,27.2732379 C43.2717697,27.9277076 43.7897242,28.4456621 44.444997,28.7315409 C45.0512848,28.9957379 45.4439667,29.5931924 45.4439667,30.2548894 C45.4439667,30.9157833 45.0512848,31.5140409 44.4441939,31.7774348 C43.7897242,32.0633136 43.2717697,32.5812682 42.9858909,33.2365409 C42.7233,33.8420258 42.1250424,34.2339045 41.4633455,34.2339045 Z M44.4538303,5.87890455 C45.4736788,6.44985909 46.3104364,7.28661667 46.8813909,8.30405606 C47.4531485,7.28581364 48.2899061,6.44825303 49.3097545,5.87729848 C48.289103,5.30714697 47.4523455,4.46958636 46.8813909,3.45054091 C46.7906485,3.61195 46.6934818,3.76854091 46.5906939,3.92031364 C46.034997,4.73458636 45.3050424,5.40190455 44.4538303,5.87890455 Z M3.40372424,37.0517379 C5.05475455,37.8258591 6.37172424,39.1428288 7.14584545,40.7938591 C7.91996667,39.1428288 9.23773939,37.8258591 10.8879667,37.0517379 C9.23773939,36.2776167 7.91996667,34.960647 7.14584545,33.3104197 C6.37172424,34.960647 5.05475455,36.2776167 3.40372424,37.0517379 Z M9.01048182,46.9145561 C8.62984545,46.9145561 8.28373939,46.7210258 8.08458788,46.3966015 C7.43493636,45.3366015 6.79813333,44.1529348 6.19184545,42.8769197 C5.77105758,42.6601015 5.43699697,42.302753 5.2442697,41.8594803 C4.67572424,40.5545561 3.6438303,39.5218591 2.33810303,38.9525106 C1.56960303,38.61845 1.09260303,37.8901015 1.09260303,37.0517379 C1.09260303,36.2133742 1.56960303,35.4850258 2.33810303,35.1509652 C2.60470909,35.0345258 2.86408788,34.8964045 3.11142121,34.7390106 C1.16487576,28.2505258 0.0326030303,20.9734652 0.000481818182,14.7339197 C-0.0203969697,10.8006773 1.50375455,7.08585909 4.29187576,4.27364697 C7.01414848,1.52969242 10.6036939,0.0119651515 14.4012242,-8.03030303e-05 C16.9476333,-8.03030303e-05 19.4129364,0.649571212 21.5746939,1.87820758 C23.0410273,2.71095 24.7161485,3.15101061 26.4193758,3.15101061 C28.122603,3.15101061 29.7977242,2.71095 31.2640576,1.87820758 C33.4266182,0.649571212 35.8943303,-8.03030303e-05 38.402997,-8.03030303e-05 C40.8249364,0.0071469697 43.1850424,0.627889394 45.2841636,1.79870758 C45.6166182,1.2349803 46.2100576,0.893692424 46.8821939,0.893692424 C47.6338303,0.893692424 48.2883,1.32170758 48.5878303,2.01151061 C49.0110273,2.98157121 49.7779212,3.74926818 50.7487848,4.17166212 C51.4377848,4.47199545 51.8674061,5.12566212 51.8674061,5.87810152 C51.8674061,6.55104091 51.5237091,7.14528333 50.9583758,7.47773788 C52.2030727,9.69731364 52.8519212,12.198753 52.8398758,14.7339197 C52.8326485,16.0709652 52.7756333,17.4642227 52.6720424,18.875147 C52.6302848,19.4388742 52.1548909,19.8805409 51.5895576,19.8805409 C51.2201636,19.857253 50.9567697,19.72395 50.7664515,19.5047227 C50.5769364,19.2846924 50.4845879,19.0044348 50.5062697,18.7153439 C50.6058455,17.3574197 50.6604515,16.01395 50.6668758,14.7226773 C50.6781182,12.64845 50.1673909,10.5975106 49.1876939,8.76419242 C48.9435727,9.06131364 48.7428152,9.38975303 48.5886333,9.74549545 C48.287497,10.4352985 47.6338303,10.8633136 46.8821939,10.8633136 C46.1297545,10.8633136 45.4768909,10.4352985 45.1765576,9.74549545 C44.7525576,8.77463182 43.9848606,8.00773788 43.013997,7.58454091 C42.324997,7.28340455 41.8969818,6.62973788 41.8969818,5.87810152 C41.8969818,5.12566212 42.3258,4.47199545 43.015603,4.17166212 C43.3721485,4.01587424 43.702997,3.81431364 43.9993152,3.57019242 C42.2800273,2.66035909 40.363997,2.17773788 38.431103,2.17131364 L38.4303,2.17131364 C36.2653303,2.17131364 34.1726333,2.72299545 32.3369061,3.76613182 C32.3047848,3.78460152 32.2718606,3.80226818 32.2389364,3.82073788 C32.6171636,4.28729848 32.9022394,4.73699545 33.2017697,5.20757121 L33.2716333,5.31678333 C33.5936485,5.82269242 33.4442848,6.49482879 32.9391788,6.81684394 C32.7641182,6.92846515 32.5633606,6.98628333 32.3569818,6.98628333 C31.9827697,6.98628333 31.6398758,6.79837424 31.4399212,6.48358636 L31.3692545,6.37276818 C30.9838,5.76567727 30.6714212,5.27502576 30.1454364,4.73057121 C28.944103,5.12164697 27.6905727,5.31999545 26.4193758,5.31999545 C25.1473758,5.31999545 23.8954515,5.12164697 22.6933152,4.73057121 C22.1665273,5.27582879 21.8557545,5.76567727 21.469497,6.37276818 L21.3980273,6.48438939 C21.1988758,6.79837424 20.8559818,6.98628333 20.4825727,6.98628333 C20.2761939,6.98628333 20.0746333,6.92846515 19.8995727,6.81684394 C19.6546485,6.66105606 19.4852091,6.41934394 19.4225727,6.13587424 C19.3599364,5.85320758 19.4113303,5.56251061 19.5663152,5.31758636 L19.6369818,5.20757121 C19.9381182,4.73378333 20.2231939,4.28569242 20.5998152,3.82073788 C20.5668909,3.80226818 20.5339667,3.78460152 20.5018455,3.76613182 C18.6661182,2.72299545 16.5718152,2.17131364 14.446997,2.17131364 C11.1915121,2.18175303 8.14642121,3.4714197 5.83369394,5.8034197 C3.45511818,8.20207121 2.15420909,11.3700258 2.17107273,14.7226773 C2.20078485,20.3736015 3.1772697,26.9873591 4.85961818,32.974753 C5.00657273,32.7402682 5.13505758,32.496147 5.2442697,32.2439955 C5.57913333,31.4754955 6.30748182,30.9984955 7.14584545,30.9984955 C7.98420909,30.9984955 8.71175455,31.4754955 9.04661818,32.2439955 C9.61516364,33.5497227 10.6470576,34.5816167 11.9535879,35.1509652 C12.7220879,35.4858288 13.1990879,36.2141773 13.1990879,37.0517379 C13.1990879,37.8901015 12.7220879,38.61845 11.9535879,38.9525106 C10.6470576,39.5218591 9.61516364,40.5545561 9.04661818,41.8602833 C8.91652727,42.1582076 8.72460303,42.4167833 8.48208788,42.6215561 C8.9542697,43.5707379 9.44251212,44.4580864 9.93637576,45.2611167 C10.2487545,45.7718439 10.0881485,46.4415712 9.57742121,46.754753 C9.40717879,46.85995 9.21043636,46.9145561 9.01048182,46.9145561 Z M36.7158303,52.5060561 C32.9472091,52.4907985 29.8675879,49.3565712 29.8499212,45.5196924 C29.8250273,40.2044348 27.3508909,40.2044348 26.4193758,40.2044348 C25.4878606,40.2044348 23.0137242,40.2044348 22.9888303,45.5196924 C22.9711636,49.3565712 19.8915424,52.4907985 16.1245273,52.5060561 C14.2558758,52.5060561 12.5245424,51.78895 11.2220273,50.4872379 C10.9088455,50.173253 10.5876333,49.8239348 10.2696333,49.4497227 C10.0809212,49.2288894 9.99098182,48.9478288 10.0142697,48.6587379 C10.0383606,48.369647 10.1732697,48.1070561 10.394103,47.919147 C10.5900424,47.7529197 10.8405879,47.6613742 11.0967545,47.6613742 C11.4171636,47.6613742 11.7183,47.8011015 11.9238758,48.0444197 C12.2009212,48.369647 12.4811788,48.6747985 12.7582242,48.9518439 C13.6503909,49.8432076 14.8356636,50.3346621 16.0964212,50.3346621 C18.6958303,50.3242227 20.8045879,48.159253 20.8174364,45.509253 C20.8455424,39.3299348 23.8793909,38.0322379 26.4193758,38.0322379 C28.9593606,38.0322379 31.9940121,39.3299348 32.0221182,45.509253 C32.0341636,48.159253 34.1429212,50.3242227 36.7238606,50.3346621 C38.0038909,50.3346621 39.1883606,49.8432076 40.0805273,48.9518439 C44.6754667,44.3577076 48.7243455,33.6075409 50.1585576,22.2021015 C50.2260121,21.6600561 50.6885576,21.2505106 51.2338152,21.2505106 C51.2795879,21.2505106 51.3253606,21.2537227 51.3703303,21.2593439 C51.6586182,21.2954803 51.9147848,21.4416318 52.0930576,21.6712985 C52.2713303,21.9009652 52.3484212,22.1852379 52.3114818,22.4719197 C51.6176636,28.0047985 50.2396636,33.7392379 48.4328455,38.61845 C46.4887091,43.8678591 44.1318152,47.9713439 41.6159212,50.4872379 C40.3142091,51.78895 38.5836788,52.5060561 36.7431333,52.5060561 L36.7158303,52.5060561 Z" id="Fill-1" fill="url(#linearGradient-3)" mask="url(#mask-2)"></path>\n' +
                    '                </g>\n' +
                    '            </g>\n' +
                    '        </g>\n' +
                    '    </g>\n' +
                    '</svg>',
                title: 'Стоматология',

            },
        ]
    },
    methods: {
        selectItemDesktop: function (item, index) {
            this.activeDesktop = index
        },
        selectItemMobile: function (item) {
            item.open = !item.open
            this.activeMobile = item.open;
            console.log(this.activeMobile)
        }
    }
});

new Vue({
    el: '.question-childbirth',
    data: {
        activeItem: 1,
        show: 0,
        textBtn: 'Читать',
        items: [
            {
                id: 1,
                title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit?',
                text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \n' +
                    'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            },
            {
                id: 2,
                title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit?',
                text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \n' +
                    'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            },
            {
                id: 3,
                title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit?',
                text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \n' +
                    'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            }
        ]
    },
    methods: {
        viewItem: function () {
            this.show = !this.show;
            if (this.show == 0) {
                this.textBtn = 'Читать'
            } else {
                this.textBtn = ' Cкрыть'
            }
        },
        selectItem: function (id) {
            this.activeItem = id;
        }
    }
});
